﻿using System;
using System.IO;
using System.Xml;
using System.Xml.Xsl;

namespace Introduction.SaveLibrary
{
    public class SaveLibraryWithXslt
    {
        public void SaveLibraryInNewFormat(string pathToSerializableLibrary, string pathToXslt, string pathToOutputResult)
        {
            if (string.IsNullOrEmpty(pathToSerializableLibrary))
            {
                throw new ArgumentException("Необходимо ввести путь к файлу, полученному в результате сериализации библиотечных объектов в xml", nameof(pathToSerializableLibrary));
            }
            if (string.IsNullOrEmpty(pathToXslt))
            {
                throw new ArgumentException("Необходимо ввести путь до xslt преобразователя в необходимый формат", nameof(pathToXslt));
            }
            if (string.IsNullOrEmpty(pathToOutputResult))
            {
                throw new ArgumentException("Необходимо ввести путь к файлу, куда должен быть сохранен результат", nameof(pathToOutputResult));
            }
            var xslt = new XslCompiledTransform();
            xslt.Load(pathToXslt);
            var settings = new XmlWriterSettings
            { 
                ConformanceLevel = ConformanceLevel.Fragment,
                Indent = true
            };
            var currentDateTime = DateTime.Now;
            var argList = new XsltArgumentList();
            argList.AddParam("dateTime", "", currentDateTime.ToString("u"));
            argList.AddParam("date", "", currentDateTime.Date.ToString("d"));
            using (var writer = XmlWriter.Create(new FileStream(pathToOutputResult, FileMode.Create), settings))
            {
                xslt.Transform(pathToSerializableLibrary, argList, writer);
            }
        }
    }
}
