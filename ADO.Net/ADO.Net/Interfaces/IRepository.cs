﻿using System.Collections.Generic;

namespace ADO.Net.Interfaces
{
    public interface IRepository<T> where T : class
    {
        IEnumerable<T> FindAll();
        T FindById(int id);
        int Create(T item);
        void Update(T item);
        void Delete(int id);
    }
}
