﻿using FluentValidation;
using Introduction.LibraryModels;
using System;

namespace Introduction.Validators
{
    public class FluentPatentValidator : AbstractValidator<Patent>
    {
        public FluentPatentValidator()
        {
            Include(new FluentLibrariItemValidator());
            RuleFor(item => item.Inventor).NotEmpty();
            RuleFor(item => item.Country).NotEmpty();
            DateTime dateControl = new (1950, 1, 1);
            RuleFor(item => item.ApplicationSubmissionDate).NotEmpty().GreaterThanOrEqualTo(dateControl);
            RuleFor(item => item.DateOfPublication).NotEmpty().GreaterThanOrEqualTo(dateControl);
            RuleFor(item => item.RegistrationNumber).Matches(@"^(((RE|PP|AI)\d{6})|((D|X|H|T)\d{7})|(\d{8})|(\d{1,10}-\d{4}\/\d{1,10}))$").WithMessage("Номер патента должне иметь длинну 8 символов и может начинаться с букв RE или PP или D или AI или X или H или T, далее дополняется цифрами до 8 символов либо без букв и только 8 цифр, либо же иметь формат <номер> -<год>/<редакция>, где номер и редакция – целые числа, год представлен в формате 4 цифр");
        }
    }
}
