using ADO.Net.Enums;
using ADO.Net.Interfaces;
using ADO.Net.Models;
using Microsoft.Extensions.Configuration;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using System.IO;
using System.Linq;

namespace ADO.Net.Test.DalTest
{
    public abstract class OrderRepositoriesTest
    {
        protected IOrderRepository Repository { get; set; }
        protected DbProviderFactory _providerFactory;
        protected string _connectionString;

        public OrderRepositoriesTest()
        {
            var builder = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json");
            var config = builder.Build();
            _connectionString = config.GetConnectionString("DefaultBd");
            var providerName = config.GetConnectionString("ProviderName");
            DbProviderFactories.RegisterFactory(providerName, SqlClientFactory.Instance);
            _providerFactory = DbProviderFactories.GetFactory(providerName);
        }

        [Test]
        public void FindAll__ReturnAllOrders()
        {
            var result = Repository.FindAll();
            Assert.IsNotEmpty(result);
        }

        [Test]
        public void FindById_ExistingId_ReturnOrderWithDetail()
        {
            var orderId = 10248;
            var result = Repository.FindById(orderId);
            Assert.AreEqual(orderId, result.OrderID);
            Assert.IsNotEmpty(result.OrderDetails);
            foreach (var orderDetail in result.OrderDetails)
            {
                Assert.IsNotEmpty(orderDetail.Product.ProductName);
            }
        }

        [Test]
        public void FindById_NotExistingId_ReturnNull()
        {
            var orderId = 0;
            var result = Repository.FindById(orderId);
            Assert.IsNull(result);
        }

        [Test]
        public void CustOrderHist_ExistingCustomerId_ReturnListCustOrderHist()
        {
            var customerId = "TOMSP";
            var result = Repository.CustOrderHist(customerId);
            Assert.IsNotEmpty(result);
        }

        [Test]
        public void CustOrderHist_NotExistingCustomerId_ReturnEmtyModels()
        {
            var customerId = "AAAAA";
            var result = Repository.CustOrderHist(customerId);
            Assert.IsEmpty(result);
        }

        [Test]
        public void CustOrdersDetail_ExistingOrderId_ReturnListCustOrdersDetail()
        {
            var orderId = 10248;
            var result = Repository.CustOrdersDetail(orderId);
            Assert.IsNotEmpty(result);
        }

        [Test]
        public void CustOrdersDetail_NotExistingOrderId_ReturnEmtyModels()
        {
            var orderId = 1;
            var result = Repository.CustOrdersDetail(orderId);
            Assert.IsEmpty(result);
        }

        [Test]
        public void Delete_OrderWithStatusInProgress_OrderHasBeenDeleted()
        {
            var firstOrderIdWithStatusInProgres = Repository.FindAll().FirstOrDefault(order => order.OrderStatus == OrderStatus.InProgress);
            
            var firstOrderIdWithStatusInProgress = Repository.FindAll().FirstOrDefault(order => order.OrderStatus == OrderStatus.InProgress).OrderID;
            var oldCountOrders = Repository.FindAll().Count();
            Repository.Delete(firstOrderIdWithStatusInProgress);
            Assert.AreNotEqual(oldCountOrders, Repository.FindAll().Count());
        }

        [Test]
        public void Delete_OrderWithStatusNew_OrderHasBeenDeleted()
        {
            var orderId = CreateOrderWithNullOrderDateStatusNew();
            var oldCountOrders = Repository.FindAll().Count();
            Repository.Delete(orderId);
            Assert.AreNotEqual(oldCountOrders, Repository.FindAll().Count());
        }

        [Test]
        public void Create_WithTwoOrderDetails_OrderHasBeenCreated()
        {
            var firstDetails = new OrderDetails
            {
                ProductID = 1,
                Quantity = 1,
                Discount = 0,
                UnitPrice = 1.1m
            };
            var secondDetails = new OrderDetails
            {
                ProductID = 2,
                Quantity = 2,
                Discount = 0,
                UnitPrice = 2.2m
            };
            var newOrder = new Order
            {
                CustomerID = "VINET",
                EmployeeID = 5,
                RequiredDate = new DateTime(2000, 2, 2),
                ShipVia = 1,
                Freight = 1m,
                ShipAddress = "Address",
                ShipCity = "City",
                ShipRegion = "Region",
                ShipPostalCode = "PostalCode",
                ShipCountry = "Country",
                OrderDetails = new List<OrderDetails> { firstDetails, secondDetails }
            };
            var oldCountOrders = Repository.FindAll().Count();
            var addedOrderId = Repository.Create(newOrder);
            var addedOrder = Repository.FindById(addedOrderId);
            Assert.AreNotEqual(oldCountOrders, Repository.FindAll().Count());
            Assert.IsNotEmpty(addedOrder.OrderDetails);
            foreach (var orderDetail in addedOrder.OrderDetails)
            {
                Assert.IsNotEmpty(orderDetail.Product.ProductName);
            }
        }

        [Test]
        public void Update_OrderWithStatusNewAndUpdateShipName_OrderHasBeenUpdated()
        {
            var orderIdForUpdate = CreateOrderWithNullOrderDateStatusNew();
            var orderForUpdate = Repository.FindById(orderIdForUpdate);
            var oldShipName = orderForUpdate.ShipName;
            orderForUpdate.ShipName = "NewName";
            Repository.Update(orderForUpdate);
            Assert.AreNotEqual(oldShipName, Repository.FindById(orderIdForUpdate).ShipName);
        }

        [Test]
        public void Update_OrderWithStatusNewAndUpdateQuantityInFirstOrderDetails_OrderHasBeenUpdated()
        {
            var orderIdForUpdate = CreateOrderWithNullOrderDateStatusNew();
            var orderForUpdate = Repository.FindById(orderIdForUpdate);
            var firstOrderDetails = orderForUpdate.OrderDetails.First();
            var oldQuantityInFirstOrderDetails = firstOrderDetails.Quantity;
            firstOrderDetails.Quantity = 10;
            Repository.Update(orderForUpdate);
            Assert.AreNotEqual(oldQuantityInFirstOrderDetails, Repository.FindById(orderIdForUpdate).OrderDetails.First().Quantity);
        }

        [Test]
        public void SetStatusInProgress_OrderWithStatusNew_OrderDateHasBeenUpdated()
        {
            var orderId = CreateOrderWithNullOrderDateStatusNew();
            Repository.SetStatusInProgress(orderId, DateTime.Now);
            Assert.IsNotNull(Repository.FindById(orderId).OrderDate);
        }

        [Test]
        public void SetStatusInProgress_OrderWithStatusNew_OrderStatusInProgress()
        {
            var orderId = CreateOrderWithNullOrderDateStatusNew();
            Repository.SetStatusInProgress(orderId, DateTime.Now);
            Assert.AreEqual(OrderStatus.InProgress, Repository.FindById(orderId).OrderStatus);
        }

        [Test]
        public void SetStatusCompleted_OrderWithStatusInProgress_OrderDateHasBeenUpdated()
        {
            var firstOrderIdWithStatusInProgress = Repository.FindAll().FirstOrDefault(order => order.OrderStatus == OrderStatus.InProgress).OrderID;
            Repository.SetStatusCompleted(firstOrderIdWithStatusInProgress, DateTime.Now);
            Assert.IsNotNull(Repository.FindById(firstOrderIdWithStatusInProgress).ShippedDate);
        }

        [Test]
        public void SetStatusCompleted_OrderWithStatusInProgress_OrderStatusCompleted()
        {
            var firstOrderIdWithStatusInProgress = Repository.FindAll().FirstOrDefault(order => order.OrderStatus == OrderStatus.InProgress).OrderID;
            Repository.SetStatusCompleted(firstOrderIdWithStatusInProgress, DateTime.Now);
            Assert.AreEqual(OrderStatus.Completed, Repository.FindById(firstOrderIdWithStatusInProgress).OrderStatus); ;
        }

        private int CreateOrderWithNullOrderDateStatusNew()
        {
            var firstDetails = new OrderDetails
            {
                ProductID = 1,
                Quantity = 1,
                Discount = 0,
                UnitPrice = 1.1m
            };
            var secondDetails = new OrderDetails
            {
                ProductID = 2,
                Quantity = 2,
                Discount = 0,
                UnitPrice = 2.2m
            };
            var newOrder = new Order
            {
                CustomerID = "VINET",
                EmployeeID = 5,
                ShipVia = 1,
                Freight = 1m,
                ShipName = "ForNullOrderDate",
                ShipAddress = "Address",
                ShipCity = "City",
                ShipRegion = "Region",
                ShipPostalCode = "PostalCode",
                ShipCountry = "Country",
                OrderDetails = new List<OrderDetails> { firstDetails, secondDetails }
            };
            return Repository.Create(newOrder);
        }
    }
}