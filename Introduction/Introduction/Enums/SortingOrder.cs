﻿namespace Introduction.Enums
{
    public enum SortingOrder
    {
        ByAscending,
        ByDescending
    }
}
