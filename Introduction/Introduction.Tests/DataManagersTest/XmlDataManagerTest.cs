﻿using Introduction.DataManagers;
using Introduction.LibraryModels;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Introduction.Tests.DataManagersTest
{
    public class XmlDataManagerTest
    {
        private XmlDataManager<LibraryItem> _xmlConverter;
        private Library _library;
        private static readonly string _pathDirectory = Directory.GetCurrentDirectory();
        private readonly string _pathToXsd = GetPathToXmlSchemaLibrary();

        [SetUp]
        public void Setup()
        {
            _xmlConverter = new XmlDataManager<LibraryItem>(_pathToXsd);
            _library = new Library();
        }

        private void Init()
        {
            var tempBook = new Book
            {
                Name = "Книга",
                Authors = { "Пушкин", "Лермонтов" },
                CityOfPublication = "Ижевск",
                NameOfThePublishingHouse = "Печать",
                YearOfPublication = 2000,
                PageCount = 50,
                Note = "примечание",
                Price = 100,
                NumberOfInstances = 5,
                Isbn = "978-1-12-123456-1"
            };
            _library.AddItem(tempBook);
            var tempNewspaper = new Newspaper
            {
                Name = "Газета",
                PageCount = 50,
                Note = "примечание",
                Price = 100,
                NameOfThePublishingHouse = "Печать",
                Number = 3,
                YearOfPublication = 2000,
                NumberOfInstances = 5,
                ReleaseDate = new DateTime(2000, 7, 20),
                Issn = "1234-1234"
            };
            _library.AddItem(tempNewspaper);
            var tempPatent = new Patent
            {
                Name = "Патент",
                PageCount = 50,
                Note = "примечание",
                Price = 100,
                Inventor = { "Тесла" },
                Country = "америка",
                DateOfPublication = new(2000, 7, 20),
                ApplicationSubmissionDate = new(2002, 7, 20),
                RegistrationNumber = "12345678"
            };
            _library.AddItem(tempPatent);
            tempPatent = new Patent
            {
                Name = "Патент2",
                PageCount = 50,
                Note = "примечание",
                Price = 100,
                Inventor = { "Тесла" },
                Country = "америка",
                DateOfPublication = new(2000, 7, 20),
                ApplicationSubmissionDate = new(2002, 7, 20),
                RegistrationNumber = "12345678"
            };
            _library.AddItem(tempPatent);
        }

        private static string GetPathToXmlSchemaLibrary()
        {
            var nameXsd = "XmlSchemaLibrary.xsd";
            var folderNameToXsd = "Xsd";
            return Path.Combine(_pathDirectory, folderNameToXsd, nameXsd);
        }

        [Test]
        public void SaveData_InitAndWriteItemsInXmlFile_CatalogItemsSerializeInXmlFile()
        {
            var fileName = "testSave.xml";
            var fullPath = Path.Combine(_pathDirectory, fileName);
            Init();
            _xmlConverter.SaveData(_library.CatalogItems, fullPath);
            FileAssert.Exists(fullPath);
        }

        [Test]
        public void GetData_InitAndWriteItemsInXmlFileThenDeserialize_XmlFileWasDeserialized()
        {
            var fileName = "testGet.xml";
            var fullPath = Path.Combine(_pathDirectory, fileName);
            Init();
            _xmlConverter.SaveData(_library.CatalogItems, fullPath);
            var resultLibrary = (_xmlConverter.GetData(fullPath)).ToList();
            for (var i = 0; i < _library.CatalogItems.Count; i++)
            {
                var expectedTypeItem = _library.CatalogItems[i].GetType();
                foreach (var property in expectedTypeItem.GetProperties())
                {
                    var expectedItemPropertyValue = property.GetValue(_library.CatalogItems[i]);
                    var resultItemPropertyValue = property.GetValue(resultLibrary.ElementAt(i));
                    if (expectedItemPropertyValue is IEnumerable<string> expected)
                    {
                        Assert.True(expected.SequenceEqual((IEnumerable<string>)resultItemPropertyValue));
                    }
                    else
                    {
                        Assert.AreEqual(expectedItemPropertyValue, resultItemPropertyValue);
                    }
                }
            }
        }

        [Test]
        public void GetData_IncorrectXml_ThrowsInvalidOperationException()
        {
            var fileName = "incorrectXml.xml";
            var folderName = "XmlForTest";
            var pathToFile = Path.Combine(_pathDirectory, folderName, fileName);
            Assert.Throws<InvalidOperationException>(() =>
            {
                (_xmlConverter.GetData(pathToFile)).ToList();
            });
        }
    }
}
