use Northwind; 
GO

create or alter procedure FindAllOrders
as begin
	select OrderID, CustomerID, EmployeeID, OrderDate, ShippedDate from dbo.Orders;
end;
GO

create or alter procedure FindOrderAndDetailsById(@OrderID int)
as begin
    select  OrderID, CustomerID, EmployeeID, OrderDate, ShippedDate, RequiredDate, ShipVia, Freight, ShipName, ShipAddress, ShipCity, ShipRegion, ShipPostalCode, ShipCountry 
	from dbo.Orders where OrderID = @OrderID;
	select ordd.OrderID, ordd.ProductID, ordd.UnitPrice, ordd.Quantity, ordd.Discount, prod.ProductName, prod.SupplierID, prod.CategoryID, prod.QuantityPerUnit, prod.UnitPrice, prod.UnitsInStock, prod.UnitsOnOrder, prod.ReorderLevel, prod.Discontinued 
	from dbo.[Order Details] as ordd 
	left join dbo.Products as prod on ordd.ProductID = prod.ProductID 
	where OrderID = @OrderID;
end;
GO

create or alter procedure FindOrderById(@OrderID int)
as begin
    select  OrderID, CustomerID, EmployeeID, OrderDate, ShippedDate, RequiredDate, ShipVia, Freight, ShipName, ShipAddress, ShipCity, ShipRegion, ShipPostalCode, ShipCountry 
	from dbo.Orders where OrderID = @OrderID;
end;
GO

create or alter procedure FindOrderDetailsById(@OrderID int)
as begin
    select ordd.OrderID, ordd.ProductID, ordd.UnitPrice, ordd.Quantity, ordd.Discount, prod.ProductName, prod.SupplierID, prod.CategoryID, prod.QuantityPerUnit, prod.UnitPrice, prod.UnitsInStock, prod.UnitsOnOrder, prod.ReorderLevel, prod.Discontinued 
	from dbo.[Order Details] as ordd 
	left join dbo.Products as prod on ordd.ProductID = prod.ProductID 
	where OrderID = @OrderID;
end;
GO

create type dbo.ListOrderDetails as table
(
	DetailProductID int NOT NULL,
	DetailUnitPrice money NOT NULL,
	DetailQuantity smallint NOT NULL,
	DetailDiscount real NOT NULL
);
GO

create or alter procedure CreateOrder(@CustomerID nchar(5), @EmployeeID int, 
  @RequiredDate datetime, @ShipVia int, @Freight money,
  @ShipName nvarchar(40), @ShipAddress nvarchar(60),@ShipCity nvarchar(15),
  @ShipRegion nvarchar(15), @ShipPostalCode nvarchar(10), @ShipCountry nvarchar(15),
  @List as dbo.ListOrderDetails readonly, @InsertOrderID int output)
as begin
declare @orderID table (ID int);
begin transaction;
	insert into dbo.Orders(CustomerID, EmployeeID,
	RequiredDate,  ShipVia, Freight, ShipName, ShipAddress,
	ShipCity, ShipRegion, ShipPostalCode, ShipCountry
	)
    output inserted.OrderID into @orderID
    values(@CustomerID, @EmployeeID, @RequiredDate, 
	@ShipVia, @Freight, @ShipName, @ShipAddress,
	@ShipCity, @ShipRegion, @ShipPostalCode, @ShipCountry
	);
	set @InsertOrderID = (select top 1 ID from @orderId);
	set nocount on;
	insert into dbo.[Order Details](OrderID, ProductID, UnitPrice, Quantity, Discount)
	select @InsertOrderID, DetailProductID, DetailUnitPrice, DetailQuantity, DetailDiscount
	from @List;
commit transaction;
end;
GO

create or alter procedure UpdateOrder(@OrderID int, @CustomerID nchar(5), @EmployeeID int,
  @RequiredDate datetime, @ShipVia int, @Freight money,
  @ShipName nvarchar(40), @ShipAddress nvarchar(60), @ShipCity nvarchar(15),
  @ShipRegion nvarchar(15), @ShipPostalCode nvarchar(10), @ShipCountry nvarchar(15),
  @List as dbo.ListOrderDetails readonly)
as begin
begin transaction;
	update dbo.Orders
    set CustomerID = @CustomerID, EmployeeID = @EmployeeID, RequiredDate = @RequiredDate, ShipVia = ShipVia,
    Freight = @Freight,ShipName = @ShipName,ShipAddress = @ShipAddress,ShipCity = @ShipCity, ShipRegion = @ShipRegion, ShipPostalCode = @ShipPostalCode, ShipCountry = @ShipCountry
		where OrderID = @OrderID;
	delete 
	from dbo.[Order Details] 
	where OrderID = @OrderID;
	insert into dbo.[Order Details](OrderID, ProductID, UnitPrice, Quantity, Discount)
	select @OrderID, DetailProductID, DetailUnitPrice, DetailQuantity, DetailDiscount
	from @List;
commit transaction;
end;
GO

create or alter procedure DeleteOrder(@OrderID int)
as begin
begin transaction;
	delete 
	from dbo.[Order Details] 
	where OrderID = @OrderID;
	delete 
	from dbo.Orders 
	where OrderID = @OrderID;
commit transaction;
end;
GO

create or alter procedure SetOrderStatusInProgress(@OrderID int, @NewDate datetime)
as begin
	update dbo.Orders 
    set OrderDate = @NewDate 
    where OrderID = @OrderID;
end;
GO

create or alter procedure SetOrderStatusCompleted(@OrderID int, @NewDate datetime)
as begin
	update dbo.Orders 
    set ShippedDate = @NewDate 
    where OrderID = @OrderID;
end;
GO