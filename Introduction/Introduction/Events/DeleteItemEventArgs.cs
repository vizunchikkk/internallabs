﻿using System;
using System.ComponentModel;

namespace Introduction.Events
{
    public class DeleteItemEventArgs : CancelEventArgs
    {
        public Type DeletedItemType { get; set; }
        public Guid DeletedItemId { get; set; }
        public string DeletedItemName { get; set; }
    }
}
