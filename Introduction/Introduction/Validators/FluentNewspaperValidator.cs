﻿using FluentValidation;
using Introduction.LibraryModels;

namespace Introduction.Validators
{
    public class FluentNewspaperValidator : AbstractValidator<Newspaper>
    {
        public FluentNewspaperValidator()
        {
            Include(new FluentLibrariItemValidator());
            RuleFor(item => item.NameOfThePublishingHouse).NotEmpty();
            RuleFor(item => item.Number).GreaterThan(0);
            RuleFor(item => item.ReleaseDate).NotEmpty();
            RuleFor(item => item.NumberOfInstances).GreaterThan(0);
            RuleFor(item => item.Issn).Matches(@"^\d{4}-\d{3}(\d|X)$").WithMessage("ISSN должн иметь длинну 8, формат 4цифры-4цифры(последний символ может быть X)");
        }
    }
}
