﻿using Introduction.Interfaces;
using Introduction.LibraryModels;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Introduction.Results;

namespace Introduction.Validators
{
    public class DataAnnotationsValidator : IValidator
    {
        public ResultOfValidation IsValidItem(LibraryItem item)
        {
            var validationResults = new List<ValidationResult>();
            var validationContext = new ValidationContext(item, null, null);
            var isValid = Validator.TryValidateObject(item, validationContext, validationResults, true);
            var resultOfValidation = new ResultOfValidation
            {
                ErrorMessages = validationResults.Select(result => result.ErrorMessage).ToList(),
                IsValid = isValid
            };
            return resultOfValidation;
        }
    }
}
