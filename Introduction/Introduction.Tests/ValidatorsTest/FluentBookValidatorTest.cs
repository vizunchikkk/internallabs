﻿using NUnit.Framework;
using FluentValidation.TestHelper;
using Introduction.Validators;
using Introduction.LibraryModels;
using System.Collections.Generic;

namespace Introduction.Tests.ValidatorsTest
{
    public class FluentBookValidatorTest
    {
        private FluentBookValidator _bookValidator;
        private Book _bookCorrect;

        [SetUp]
        public void Setup()
        {
            _bookValidator = new FluentBookValidator();
            _bookCorrect = new Book
            {
                Name = "Книга",
                Authors = { "Пушкин" },
                CityOfPublication = "Ижевск",
                NameOfThePublishingHouse = "Печать",
                YearOfPublication = 2000,
                PageCount = 50,
                Note = "примечание",
                Price = 100,
                NumberOfInstances = 5,
                Isbn = "978-1-12-123456-1"
            };
        }

        //тестирую общие свойства из LibraryItem только 1 раз в этом классе тестов
        [Test]
        public void Validate_CorrectBookProperty_BookIsValid()
        {
            var result = _bookValidator.TestValidate(_bookCorrect);
            Assert.True(result.IsValid);
        }

        [TestCase(null)]
        [TestCase("")]
        public void Validate_NameNullOrEmpty_ReturnError(string name)
        {
            _bookCorrect.Name = name;
            var result = _bookValidator.TestValidate(_bookCorrect);
            result.ShouldHaveValidationErrorFor(book => book.Name);
        }

        [Test]
        public void Validate_PageCountLess1_ReturnError()
        {
            _bookCorrect.PageCount = 0;
            var result = _bookValidator.TestValidate(_bookCorrect);
            result.ShouldHaveValidationErrorFor(book => book.PageCount);
        }

        [Test]
        public void Validate_PageCount1_ReturnNotError()
        {
            _bookCorrect.PageCount = 1;
            var result = _bookValidator.TestValidate(_bookCorrect);
            result.ShouldNotHaveValidationErrorFor(book => book.PageCount);
        }

        [Test]
        public void Validate_NoteLengthMore500_ReturnError()
        {
            _bookCorrect.Note = "Однажды играли в карты у конногвардейца Нарумова. Долгая зимняя ночь прошла незаметно; сели ужинать в пятом часу утра. Те, которые остались в выигрыше, ели с большим аппетитом, прочие, в рассеянности, сидели перед пустыми своими приборами. Но шампанское явилось, разговор оживился, и все приняли в нем участие.— Что ты сделал, Сурин? — спросил хозяин.— Проиграл, по обыкновению. Надобно признаться, что я несчастлив: играю мирандолем, никогда не горячусь, ничем меня с толку не собьешь, а все проигрываюсь!— И ты ни разу не соблазнился? ни разу не поставил на руте?..Твердость твоя для меня удивительна.— А каков Германн! — сказал один из гостей, указывая на молодого инженера, — отроду не брал он карты в руки, отроду не загнул ни одного пароли, а до пяти часов сидит с нами и смотрит на нашу игру!";
            var result = _bookValidator.TestValidate(_bookCorrect);
            result.ShouldHaveValidationErrorFor(book => book.Note.Length);
        }

        [Test]
        public void Validate_NoteLength500_ReturnNotError()
        {
            _bookCorrect.Note = "Однажды играли в карты у конногвардейца Нарумова. Долгая зимняя ночь прошла незаметно; сели ужинать в пятом часу утра. Те, которые остались в выигрыше, ели с большим аппетитом, прочие, в рассеянности, сидели перед пустыми своими приборами. Но шампанское явилось, разговор оживился, и все приняли в нем участие.— Что ты сделал, Сурин? — спросил хозяин.— Проиграл, по обыкновению. Надобно признаться, что я несчастлив: играю мирандолем, никогда не горячусь, ничем меня с толку не собьешь, а все проигры";
            var result = _bookValidator.TestValidate(_bookCorrect);
            result.ShouldNotHaveValidationErrorFor(book => book.Note.Length);
        }

        [TestCase(-1)]
        [TestCase(0)]
        public void Validate_PriceLessOrEqual0_ReturnError(decimal price)
        {
            _bookCorrect.Price = price;
            var result = _bookValidator.TestValidate(_bookCorrect);
            result.ShouldHaveValidationErrorFor(book => book.Price);
        }

        [Test]
        public void Validate_PriceMore0_ReturnNotError()
        {
            _bookCorrect.Price = 0.01m;
            var result = _bookValidator.TestValidate(_bookCorrect);
            result.ShouldNotHaveValidationErrorFor(book => book.Price);
        }

        [Test]
        public void Validate_AuthorsEmpty_ReturnError()
        {
            _bookCorrect.Authors = new List<string>();
            var result = _bookValidator.TestValidate(_bookCorrect);
            result.ShouldHaveValidationErrorFor(book => book.Authors);
        }

        [Test]
        public void Validate_AuthorsCount1_ReturnNotError()
        {
            _bookCorrect.Authors = new List<string> { "a" };
            var result = _bookValidator.TestValidate(_bookCorrect);
            result.ShouldNotHaveValidationErrorFor(book => book.Authors);
        }

        [TestCase(null)]
        [TestCase("")]
        public void Validate_CityOfPublicationNullOrEmpty_ReturnError(string cityOfPublication)
        {
            _bookCorrect.CityOfPublication = cityOfPublication;
            var result = _bookValidator.TestValidate(_bookCorrect);
            result.ShouldHaveValidationErrorFor(book => book.CityOfPublication);
        }

        [TestCase(null)]
        [TestCase("")]
        public void Validate_NameOfThePublishingHouseNullOrEmpty_ReturnError(string nameOfThePublishingHouse)
        {
            _bookCorrect.NameOfThePublishingHouse = nameOfThePublishingHouse;
            var result = _bookValidator.TestValidate(_bookCorrect);
            result.ShouldHaveValidationErrorFor(book => book.NameOfThePublishingHouse);
        }

        [Test]
        public void Validate_YearOfPublicationLess1900_ReturnError()
        {
            _bookCorrect.YearOfPublication = 1899;
            var result = _bookValidator.TestValidate(_bookCorrect);
            result.ShouldHaveValidationErrorFor(book => book.YearOfPublication);
        }

        [Test]
        public void Validate_YearOfPublication1900_ReturnNotError()
        {
            _bookCorrect.YearOfPublication = 1900;
            var result = _bookValidator.TestValidate(_bookCorrect);
            result.ShouldNotHaveValidationErrorFor(book => book.YearOfPublication);
        }

        [TestCase(-1)]
        [TestCase(0)]
        public void Validate_NumberOfInstancesLessOrEqual0_ReturnError(int numberOfInstances)
        {
            _bookCorrect.NumberOfInstances = numberOfInstances;
            var result = _bookValidator.TestValidate(_bookCorrect);
            result.ShouldHaveValidationErrorFor(book => book.NumberOfInstances);
        }


        [Test]
        public void Validate_NumberOfInstances1_ReturnNotError()
        {
            _bookCorrect.NumberOfInstances = 1;
            var result = _bookValidator.TestValidate(_bookCorrect);
            result.ShouldNotHaveValidationErrorFor(book => book.NumberOfInstances);
        }

        [TestCase("-12-123456-7")]
        [TestCase("12-12-123456-7")]
        [TestCase("112-123456-7")]
        [TestCase("12-123456-7")]
        [TestCase("12-1-123456-7")]
        [TestCase("1-123-123456-7")]
        [TestCase("1-12-12345-7")]
        [TestCase("1-12-1234567-7")]
        [TestCase("1-12123456-7")]
        [TestCase("1-12-123456-")]
        [TestCase("1-12-123456-17")]
        [TestCase("1-12-123456-7-")]
        public void Validate_IsbnMustBe10DigitsAndTheyIncorrectlySeparatedDashesOrQuantityNotEqual10_ReturnError(string isbn)
        {
            _bookCorrect.Isbn = isbn;
            var result = _bookValidator.TestValidate(_bookCorrect);
            result.ShouldHaveValidationErrorFor(book => book.Isbn);
        }

        [TestCase("a-12-123456-7")]
        [TestCase("1-a2-123456-7")]
        [TestCase("1-12-12345a-7")]
        [TestCase("1-12-123456-a")]
        public void Validate_IsbnMustBe10DigitsAndTheyContainNotOnlyDigits_ReturnError(string isbn)
        {
            _bookCorrect.Isbn = isbn;
            var result = _bookValidator.TestValidate(_bookCorrect);
            result.ShouldHaveValidationErrorFor(book => book.Isbn);
        }

        [TestCase("1-12-123456-X")]
        [TestCase("1-12-123456-1")]
        public void Validate_IsbnMustBe10DigitsAndCheckDigitIsNumberOrX_ReturnNotError(string isbn)
        {
            _bookCorrect.Isbn = isbn;
            var result = _bookValidator.TestValidate(_bookCorrect);
            result.ShouldNotHaveValidationErrorFor(book => book.Isbn);
        }

        [TestCase("978-1-12-123456-1")]
        [TestCase("979-1-12-123456-1")]
        public void Validate_IsbnMustBe13DigitsAndPrefixElement978Or979_ReturnNotError(string isbn)
        {
            _bookCorrect.Isbn = isbn;
            var result = _bookValidator.TestValidate(_bookCorrect);
            result.ShouldNotHaveValidationErrorFor(book => book.Isbn);
        }

        [TestCase("1-1-12-123456-1")]
        [TestCase("12-1-12-123456-1")]
        [TestCase("123-1-12-123456-1")]
        [TestCase("9-78-1-12-123456-1")]
        [TestCase("97-8-1-12-123456-1")]
        [TestCase("977-1-12-123456-1")]
        [TestCase("97a-1-12-123456-1")]
        [TestCase("9789-1-12-123456-1")]
        public void Validate_IsbnMustBe13DigitsAndPrefixElementNot978Or979OrIncorrectlySeparatedDashes_ReturnError(string isbn)
        {
            _bookCorrect.Isbn = isbn;
            var result = _bookValidator.TestValidate(_bookCorrect);
            result.ShouldHaveValidationErrorFor(book => book.Isbn);
        }
    }
}
