﻿using Introduction.LibraryModels;
using Introduction.Reports;
using NUnit.Framework;
using System;
using System.IO;

namespace Introduction.Tests.ReportTest
{
    public class TxtReportBuilderTest
    {
        private TxtReportBuilder _report;
        private Library _library;
        private string _pathDirectory;

        [SetUp]
        public void Setup()
        {
            _report = new TxtReportBuilder(new Book(), new Newspaper(), new Patent());
            _library = new Library();
            _pathDirectory = Directory.GetCurrentDirectory();
        }

        private void Init()
        {
            var tempBook = new Book()
            {
                Name = "Пиковая дама",
                Authors = { "кто-то", "Пушкин" },
                YearOfPublication = 1834,
                NameOfThePublishingHouse = "эксмо",
                PageCount = 200,
                Note = "Примечание",
                Price = 15.5m
            };
            _library.AddItem(tempBook);
            tempBook = new Book()
            {
                Name = "Парус",
                Authors = { "Лермонтов" },
                NameOfThePublishingHouse = "Атрибут",
                Price = 5.5m
            };
            _library.AddItem(tempBook);
            tempBook = new Book()
            {
                Name = "Пушкин",
                Authors = { "Лермонтов", "Пушка" },
                YearOfPublication = 1835,
                NameOfThePublishingHouse = "Атрибут"
            };
            _library.AddItem(tempBook);
            tempBook = new Book()
            {
                Name = "Тупиковая",
                Authors = { "Пушкин" },
                YearOfPublication = 1840,
                NameOfThePublishingHouse = "Аэксмо",
                Isbn = "123456"
            };
            _library.AddItem(tempBook);
            tempBook = new Book()
            {
                Name = "Мцири",
                Authors = { "Лермонтов" },
                YearOfPublication = 1835,
                CityOfPublication = "Ижевск",
                NameOfThePublishingHouse = "Арбита"
            };
            _library.AddItem(tempBook);
            var tempNewspaper = new Newspaper()
            {
                Name = "Из рук в руки",
                CityOfPublication = "Ижевск",
                NameOfThePublishingHouse = "Арбита",
                Number = 50,
                Price = 12.5m
            };
            _library.AddItem(tempNewspaper);
            tempNewspaper = new Newspaper()
            {
                Name = "Комсомолец",
                YearOfPublication = 1840,
                Issn = "123",
                ReleaseDate = new DateTime(2018, 11, 20),
                Price = 17
            };
            _library.AddItem(tempNewspaper);
            var tempPatent = new Patent()
            {
                Name = "Лампочка",
                Inventor = { "Эдисон" },
                Country = "Америка",
                RegistrationNumber = "985",
                ApplicationSubmissionDate = new DateTime(2014, 7, 11),
                DateOfPublication = new DateTime(2015, 7, 20),
                Price = 17
            };
            _library.AddItem(tempPatent);
        }

        [Test]
        public void GenerateReport_InitAndWriteItemInFile_ReportFileWasGenerated()
        {
            var fileName = "report.txt";
            var fullPath = Path.Combine(_pathDirectory, fileName);
            Init();
            _report.GenerateReport(fullPath, _library.CatalogItems);
            FileAssert.Exists(fullPath);
        }

        [Test]
        public void GenerateReport_ListCatalogItemsEmpty_ThrowsArgumentException()
        {
            var fileName = "report.txt";
            var fullPath = Path.Combine(_pathDirectory, fileName);
            Assert.Throws<ArgumentException>(() =>
            {
                _report.GenerateReport(fullPath, _library.CatalogItems);
            });
        }

        [Test]
        public void GenerateReport_StringPathExtensionNotTxt_ThrowsArgumentException()
        {
            var fileName = "report.dock";
            var fullPath = Path.Combine(_pathDirectory, fileName);
            Init();
            Assert.Throws<ArgumentException>(() =>
            {
                _report.GenerateReport(fullPath, _library.CatalogItems);
            });
        }
    }
}
