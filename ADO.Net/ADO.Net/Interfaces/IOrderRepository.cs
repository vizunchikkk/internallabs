﻿using ADO.Net.Models;
using System;
using System.Collections.Generic;

namespace ADO.Net.Interfaces
{
    public interface IOrderRepository : IRepository<Order>
    {
        void SetStatusInProgress(int orderId, DateTime orderDate);
        void SetStatusCompleted(int orderId, DateTime shippedDate);
        IEnumerable<CustOrderHistResult> CustOrderHist(string customerId);
        IEnumerable<CustOrdersDetailResult> CustOrdersDetail(int orderId);
    }
}
