﻿using System.Collections.Generic;

namespace Introduction.Interfaces
{
    public interface IDataManager<T> where T : class
    {
        IEnumerable<T> GetData(string path); 
        void SaveData(IEnumerable<T> data, string path);
    }
}
