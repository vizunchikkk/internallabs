<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xmlns:msxsl="urn:schemas-microsoft-com:xslt"
  xml:space="preserve">
  
<xsl:output method="html" indent="yes" omit-xml-declaration="yes" />
<xsl:param name="date"/>

<xsl:template match="/">
  <html>
  <body>
    <h2>
		Current Funds <xsl:value-of select="$date"/>
	</h2>
	<h3>
		Books
	</h3>
    <table border="1">
      <tr bgcolor="#9acd32">
        <th>Id</th>
        <th>Name</th>
        <th>Note</th>
        <th>Page Count</th>
        <th>Price</th>
        <th>Year Of Publication</th>
        <th>Authors</th>
        <th>City Of Publication</th>
        <th>Isbn</th>
        <th>Name Of The Publishing House</th>
        <th>Number Of Instances</th>
      </tr>
      <xsl:for-each select="/ArrayOfLibraryItem/LibraryItem[@xsi:type='Book']">
        <tr>
          <td><xsl:value-of select="Id"/></td>
          <td><xsl:value-of select="Name"/></td>
          <td><xsl:value-of select="Note"/></td>
          <td><xsl:value-of select="PageCount"/></td>
          <td><xsl:value-of select="Price"/></td>
          <td><xsl:value-of select="YearOfPublication"/></td>
          <td><xsl:for-each select="Authors/*"><xsl:value-of select="."/> </xsl:for-each></td>
          <td><xsl:value-of select="CityOfPublication"/></td>
          <td><xsl:value-of select="Isbn"/></td>
          <td><xsl:value-of select="NameOfThePublishingHouse"/></td>
          <td><xsl:value-of select="NumberOfInstances"/></td>
        </tr>
      </xsl:for-each>
    </table>
	<h3>
		Newspapers
	</h3>
    <table border="1">
      <tr bgcolor="#9acd32">
        <th>Id</th>
        <th>Name</th>
        <th>Note</th>
        <th>Page Count</th>
        <th>Price</th>
        <th>Year Of Publication</th>
        <th>City Of Publication</th>
        <th>Issn</th>
        <th>Name Of The Publishing House</th>
        <th>Number</th>
        <th>Number Of Instances</th>
        <th>Release Date</th>
      </tr>
      <xsl:for-each select="/ArrayOfLibraryItem/LibraryItem[@xsi:type='Newspaper']">
        <tr>
          <td><xsl:value-of select="Id"/></td>
          <td><xsl:value-of select="Name"/></td>
          <td><xsl:value-of select="Note"/></td>
          <td><xsl:value-of select="PageCount"/></td>
          <td><xsl:value-of select="Price"/></td>
          <td><xsl:value-of select="YearOfPublication"/></td>
          <td><xsl:value-of select="CityOfPublication"/></td>
          <td><xsl:value-of select="Issn"/></td>
          <td><xsl:value-of select="NameOfThePublishingHouse"/></td>
          <td><xsl:value-of select="Number"/></td>
          <td><xsl:value-of select="NumberOfInstances"/></td>
          <td><xsl:value-of select="ReleaseDate"/></td>
        </tr>
      </xsl:for-each>
    </table>
	<h3>
		Patents
	</h3>
    <table border="1">
      <tr bgcolor="#9acd32">
        <th>Id</th>
        <th>Name</th>
        <th>Note</th>
        <th>Page Count</th>
        <th>Price</th>
        <th>Year Of Publication</th>
        <th>Application Submission Date</th>
        <th>Country</th>
        <th>Date Of Publication</th>
        <th>Inventors</th>
        <th>Registration Number</th>
      </tr>
      <xsl:for-each select="/ArrayOfLibraryItem/LibraryItem[@xsi:type='Patent']">
        <tr>
          <td><xsl:value-of select="Id"/></td>
          <td><xsl:value-of select="Name"/></td>
          <td><xsl:value-of select="Note"/></td>
          <td><xsl:value-of select="PageCount"/></td>
          <td><xsl:value-of select="Price"/></td>
          <td><xsl:value-of select="YearOfPublication"/></td>
          <td><xsl:value-of select="ApplicationSubmissionDate"/></td>
          <td><xsl:value-of select="Country"/></td>
          <td><xsl:value-of select="DateOfPublication"/></td>
          <td><xsl:for-each select="Inventor/*"><xsl:value-of select="."/> </xsl:for-each></td>
          <td><xsl:value-of select="RegistrationNumber"/></td>
        </tr>
      </xsl:for-each>
    </table>
	<xsl:variable name="Total" xmlns="">
		<AmountOfBooks>
			<xsl:value-of select="sum(/ArrayOfLibraryItem/LibraryItem[@xsi:type='Book']/NumberOfInstances)"/>
		</AmountOfBooks>
		<AmountOfNewspapers>
			<xsl:value-of select="sum(/ArrayOfLibraryItem/LibraryItem[@xsi:type='Newspaper']/NumberOfInstances)"/>
		</AmountOfNewspapers>
		<AmountOfPatents>
			<xsl:value-of select="count(/ArrayOfLibraryItem/LibraryItem[@xsi:type='Patent']/Name)"/>
		</AmountOfPatents>
	</xsl:variable>
	<div>Summary</div>
	<div>Books: <xsl:value-of select="msxsl:node-set($Total)/AmountOfBooks"/></div>
	<div>Newspapers: <xsl:value-of select="msxsl:node-set($Total)/AmountOfNewspapers"/></div>
	<div>Patents: <xsl:value-of select="msxsl:node-set($Total)/AmountOfPatents"/></div>
	<div>Total: <xsl:value-of select="sum(msxsl:node-set($Total)/*)" /></div>
  </body>
  </html>
</xsl:template>

</xsl:stylesheet>