﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Introduction.LibraryModels
{
    [DataContract]
    public class Book : LibraryItem
    {
        [DataMember]
        [MinLength(1, ErrorMessage = "Список авторов не может быть пустым")]
        public List<string> Authors { get; set; }
        [DataMember]
        [Required(ErrorMessage = "Место издания (город) не может быть пустым")]
        public string CityOfPublication { get; set; }
        [DataMember]
        [Required(ErrorMessage = "Название издательства не может быть пустым")]
        public string NameOfThePublishingHouse { get; set; }
        [DataMember]
        [RegularExpression(@"^(97(8|9)-)?\d-\d{2}-\d{6}-(\d|X)$", ErrorMessage = "ISBN должн иметь длинну 10 или 13(начинается с 978- или 979-), далее формат 1цифра-2цифры-6цифр-1цифра(либо X)")]
        public string Isbn { get; set; }
        [DataMember]
        [Range(1, int.MaxValue, ErrorMessage = "Количество экземпляров не может быть отрицательно")]
        public int NumberOfInstances { get; set; }

        public Book()
        {
            Authors = new List<string>();
        }
    }
}
