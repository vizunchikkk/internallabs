﻿using System;

namespace Introduction.Events
{
    public class AddItemEventArgs : EventArgs
    {
        public Type AddItemType { get; internal set; }
    }
}
