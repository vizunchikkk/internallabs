﻿using Introduction.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace Introduction.DataManagers
{
    public class XmlDataManager<T> : IDataManager<T> where T : class
    {
        private readonly Type[] _allHeirsTType;
        private readonly string[] _allHeirsTTypeString;
        private readonly XmlWriterSettings _xmlWriterSettings;
        private XmlReaderSettings _xmlReaderSettings = new();

        public XmlDataManager(string pathToXsd)
        {
            _xmlReaderSettings.Schemas.Add(null, pathToXsd);
            _xmlReaderSettings.ValidationType = ValidationType.Schema;
            _xmlReaderSettings.ValidationFlags = XmlSchemaValidationFlags.ProcessInlineSchema | XmlSchemaValidationFlags.ProcessSchemaLocation | XmlSchemaValidationFlags.ReportValidationWarnings;
            _xmlReaderSettings.ValidationEventHandler += new ValidationEventHandler(XmlLibraryValidation);

            _allHeirsTType = AppDomain.CurrentDomain.GetAssemblies().SelectMany(assembly => assembly.GetTypes()).Where(type => type.IsSubclassOf(typeof(T))).ToArray();
            _allHeirsTTypeString = _allHeirsTType.Select(type => type.Name).ToArray();
            _xmlWriterSettings = new XmlWriterSettings()
            {
                Indent = true
            };
        }

        private void XmlLibraryValidation(object sender, ValidationEventArgs e)
        {
            if (e.Severity == XmlSeverityType.Warning)
            {
                var warning = String.Format("Предупреждения xml: {0}", e.Message);
                throw new InvalidOperationException(warning);
            }
            else
            {
                var error = String.Format("Ошибки xml: {0}", e.Message);
                throw new InvalidOperationException(error);
            }
        }

        public IEnumerable<T> GetData(string pathToFile)
        {
            var xml = new XmlSerializer(typeof(T), _allHeirsTType);
            var xmlReader = XmlReader.Create(pathToFile, _xmlReaderSettings);
            while (xmlReader.Read())
            {
                if (xmlReader.NodeType == XmlNodeType.Element)
                {
                    if (IsReadStringContainsTypeHeirOfTypeT(xmlReader))
                    {
                        yield return (T)xml.Deserialize(xmlReader);
                    }
                }
            }
        }

        private bool IsReadStringContainsTypeHeirOfTypeT(XmlReader reader)
        {
            return _allHeirsTTypeString.Contains(reader.GetAttribute("xsi:type"));
        }

        public void SaveData(IEnumerable<T> items, string pathToFile)
        {
            using var stream = new FileStream(pathToFile, FileMode.Create);
            using var writer = XmlWriter.Create(stream, _xmlWriterSettings);
            var xml = new XmlSerializer(items.GetType(), _allHeirsTType);
            xml.Serialize(writer, items);
        }
    }
}
