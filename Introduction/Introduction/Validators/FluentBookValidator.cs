﻿using FluentValidation;
using Introduction.LibraryModels;

namespace Introduction.Validators
{
    public class FluentBookValidator : AbstractValidator<Book>
    {
        public FluentBookValidator()
        {
            Include(new FluentLibrariItemValidator());
            RuleFor(item => item.Authors).NotEmpty();
            RuleFor(item => item.CityOfPublication).NotEmpty();
            RuleFor(item => item.NameOfThePublishingHouse).NotEmpty();
            RuleFor(item => item.NumberOfInstances).GreaterThan(0);
            RuleFor(item => item.Isbn).Matches(@"^(97(8|9)-)?\d-\d{2}-\d{6}-(\d|X)$").WithMessage("ISBN должн иметь длинну 10 или 13(начинается с 978- или 979-), далее формат 1цифра-2цифры-6цифр-1цифра(либо X)");
        }
    }
}
