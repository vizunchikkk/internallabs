﻿using Introduction.Enums;
using Introduction.Interfaces;
using Introduction.LibraryModels;
using Introduction.Reports;
using Introduction.Validators;
using Moq;
using NUnit.Framework;
using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;

namespace Introduction.Tests
{
    public class LibraryTest
    {
        private Library _library;
        private Mock<IDataManager<LibraryItem>> _mockDataManager = new();
        private IDataManager<LibraryItem> DataManager => _mockDataManager.Object;
        private readonly string _fileName = "test.json";
        private enum TypeCatalogItems
        {
            Correct,
            NotCorrect
        }

        [SetUp]
        public void Setup()
        {
            _library = new Library();
            Init();
        }

        private void Init()
        {
            var tempBook = new Book()
            {
                Name = "Пиковая дама",
                Authors = { "кто-то", "Пушкин" },
                YearOfPublication = 1834,
                NameOfThePublishingHouse = "эксмо",
                PageCount = 200,
                Note = "Примечание",
                Price = 15.5m
            };
            _library.AddItem(tempBook);
            tempBook = new Book()
            {
                Name = "Парус",
                Authors = { "Лермонтов" },
                NameOfThePublishingHouse = "Атрибут",
                Price = 5.5m
            };
            _library.AddItem(tempBook);
            tempBook = new Book()
            {
                Name = "Пушкин",
                Authors = { "Лермонтов", "Пушка" },
                YearOfPublication = 1835,
                NameOfThePublishingHouse = "Атрибут"
            };
            _library.AddItem(tempBook);
            tempBook = new Book()
            {
                Name = "Тупиковая",
                Authors = { "Пушкин" },
                YearOfPublication = 1840,
                NameOfThePublishingHouse = "Аэксмо",
                Isbn = "123456"
            };
            _library.AddItem(tempBook);
            tempBook = new Book()
            {
                Name = "Мцири",
                Authors = { "Лермонтов" },
                YearOfPublication = 1835,
                CityOfPublication = "Ижевск",
                NameOfThePublishingHouse = "Арбита"
            };
            _library.AddItem(tempBook);
            var tempNewspaper = new Newspaper()
            {
                Name = "Из рук в руки",
                CityOfPublication = "Ижевск",
                NameOfThePublishingHouse = "Арбита",
                Number = 50,
                Price = 12.5m
            };
            _library.AddItem(tempNewspaper);
            tempNewspaper = new Newspaper()
            {
                Name = "Комсомолец",
                YearOfPublication = 1840,
                Issn = "123",
                ReleaseDate = new DateTime(2018, 11, 20),
                Price = 17
            };
            _library.AddItem(tempNewspaper);
            var tempPatent = new Patent()
            {
                Name = "Лампочка",
                Inventor = { "Эдисон" },
                Country = "Америка",
                RegistrationNumber = "985",
                ApplicationSubmissionDate = new DateTime(2014, 7, 11),
                DateOfPublication = new DateTime(2015, 7, 20),
                Price = 17
            };
            _library.AddItem(tempPatent);
        }

        [Test]
        public void AddItem_AddOneItemWithoutSubscribersEvent_LibraryExistsThisItem()
        {
            var tempBook = new Book()
            {
                Name = "Онегин",
                Authors = { "Пушкин" },
                YearOfPublication = 1834,
                NameOfThePublishingHouse = "эксмо"
            };
            _library.AddItem(tempBook);
            Assert.True(_library.CatalogItems.Exists(item => item.Id == tempBook.Id));
        }

        [Test]
        public void DeleteItem_ExistingItemInLibraryWithoutSubscribersEvent_ItemHasBeenDeleted()
        {
            var idToDelete = _library.CatalogItems[1].Id;
            _library.DeleteItem(idToDelete);
            var findDeletedItem = _library.CatalogItems.Find(item => item.Id == idToDelete);
            Assert.IsNull(findDeletedItem);
        }

        [Test]
        public void DeleteItem_NotExistingItemInLibrary_ThrowsInvalidOperationException()
        {
            Assert.Throws<InvalidOperationException>(() =>
            {
                var idToDelete = Guid.NewGuid();
                _library.DeleteItem(idToDelete);
            });
        }

        [TestCase("овая")]
        [TestCase("ghb")]
        public void SearchByPartOfTheName__ReturnsLibraryItemsThatContainsPartOfTheName(string partOfName)
        {
            var resultArray = _library.SearchByPartOfTheName(partOfName);
            Assert.True(resultArray.All(item => item.Name.Contains(partOfName)));
        }

        [TestCase("")]
        [TestCase(null)]
        public void SearchByPartOfTheName_PartOfNameNullOrEmpty_ThrowsArgumentException(string partOfName)
        {
            Assert.Throws<ArgumentException>(() =>
            {
                _library.SearchByPartOfTheName(partOfName);
            });
        }

        [Test]
        public void SortingObjectsByYearOfPublication_SortByAscending_ItemsWereSortedByAscending()
        {
            _library.SortingObjectsByYearOfPublication(SortingOrder.ByAscending);
            for (var i = 1; i < _library.CatalogItems.Count; i++)
            {
                Assert.True(_library.CatalogItems[i - 1].YearOfPublication <= _library.CatalogItems[i].YearOfPublication);
            }
        }

        [Test]
        public void SortingObjectsByYearOfPublication_SortByDescending_ItemsWereSortedByDescending()
        {
            _library.SortingObjectsByYearOfPublication(SortingOrder.ByDescending);
            for (var i = 1; i < _library.CatalogItems.Count; i++)
            {
                Assert.True(_library.CatalogItems[i - 1].YearOfPublication >= _library.CatalogItems[i].YearOfPublication);
            }
        }

        [TestCase("Пушкин")]
        [TestCase("Носов")]
        public void SearchByAuthor__ReturnsLibraryItemsThatContainsAuthor(string author)
        {
            var resultArray = _library.SearchByAuthor(author);
            Assert.True(resultArray.All(item => item.Authors.Contains(author)));
        }

        [TestCase(null)]
        [TestCase("")]
        public void SearchByAuthor_StringAuthorNullOrEmpty_ThrowsArgumentException(string author)
        {
            Assert.Throws<ArgumentException>(() =>
            {
                var _ = _library.SearchByAuthor(author);
            });
        }

        [TestCase("А")]
        [TestCase("Б")]
        public void GroupPublishingHouseBeginsWith__ReturnsGroupsGroupedByPublishingHouseWhichBeginsWith(string startNameOfThePublishingHouse)
        {
            var resultGroups = _library.GroupPublishingHouseBeginsWith(startNameOfThePublishingHouse);
            foreach (var group in resultGroups)
            {
                Assert.True(group.Key.StartsWith(startNameOfThePublishingHouse) &&
                    group.Value.All(item => item.NameOfThePublishingHouse.StartsWith(startNameOfThePublishingHouse) && 
                    item.NameOfThePublishingHouse == group.Key));
            }
        }

        [TestCase(null)]
        [TestCase("")]
        public void GroupPublishingHouseBeginsWith_StringPublishingHouseNullOrEmpty_ThrowsArgumentException
           (string startNameOfThePublishingHouse)
        {
            Assert.Throws<ArgumentException>(() =>
            {
                var _ = _library.GroupPublishingHouseBeginsWith(startNameOfThePublishingHouse);
            });
        }

        [Test]
        public void GroupByYearOfPublication__ReturnsGroupsGroupedByYearOfPublication()
        {
            var resultGroups = _library.GroupByYearOfPublication();
            foreach (var group in resultGroups)
            {
                Assert.True(group.Value.All(item => item.YearOfPublication == group.Key));
            }
        }

        [Test]
        public void UpdateItem_UpdateProperyNameAtExistingItemInLibraryAndTypeItemsMatch_ItemHasBeenUpdated()
        {
            var itemToUpdate = _library.CatalogItems[3];
            var idItemToUpdate = itemToUpdate.Id;
            var updateProperyName = "ААА";
            var newItem = new Book()
            {
                Id = idItemToUpdate,
                Name = updateProperyName,
                Authors = { "Пушкин" },
                YearOfPublication = 1840,
                NameOfThePublishingHouse = "Аэксмо",
                Isbn = "123456"
            };
            _library.UpdateItem(newItem);
            Assert.True(_library.CatalogItems.Contains(itemToUpdate) && itemToUpdate.Name.Equals(updateProperyName));
        }

        [Test]
        public void UpdateItem_NotExistingItemInLibrary_ThrowsInvalidOperationException()
        {
            Assert.Throws<InvalidOperationException>(() =>
            {

                var newItem = new Book()
                {
                    Name = "ААА",
                    Authors = { "Пушкин" },
                    YearOfPublication = 1840,
                    NameOfThePublishingHouse = "Аэксмо",
                    Isbn = "123456"
                };
                _library.UpdateItem(newItem);
            });
        }

        [Test]
        public void UpdateItem_UpdateProperyNameAtExistingItemInLibraryAndTypeItemsNotMatch_ThrowsInvalidOperationException()
        {
            Assert.Throws<InvalidOperationException>(() =>
            {
                var itemToUpdate = _library.CatalogItems[3];
                var idItemToUpdate = itemToUpdate.Id;
                var newItem = new Newspaper()
                {
                    Id = idItemToUpdate,
                    Name = "Комсомолец",
                    YearOfPublication = 1840,
                    Issn = "123",
                    ReleaseDate = new DateTime(2018, 11, 20)
                };
                _library.UpdateItem(newItem);
            });
        }

        [Test]
        public void FundAddedEvent_SubscribeAndAddItem_SubscriptionHasHappenedAndItemAdded()
        {
            var eventRaised = false;
            var tempBook = new Book()
            {
                Name = "Онегин",
                Authors = { "Пушкин" },
                YearOfPublication = 1834,
                NameOfThePublishingHouse = "эксмо"
            };
            _library.FundAdded += (sendear, e) => { eventRaised = true; };
            _library.AddItem(tempBook);
            Assert.IsTrue(eventRaised);
        }

        [Test]
        public void FundDeleting_SubscribeAndGenerateEventDeleteItem_DeletionEventWasGenerated()
        {
            var eventRaised = false;
            var idToDelete = _library.CatalogItems[1].Id;
            _library.FundDeleting += (sender, e) => { eventRaised = true; };
            _library.DeleteItem(idToDelete);
            Assert.IsTrue(eventRaised);
        }

        [Test]
        public void FundDeleting_SubscribeAndGenerateDeleteExistItemInLibrary_ItemHasBeenDeleted()
        {
            var idToDelete = _library.CatalogItems[1].Id;
            _library.FundDeleting += (sender, e) => { };
            _library.DeleteItem(idToDelete);
            var findDeletedItem = _library.CatalogItems.Find(item => item.Id == idToDelete);
            Assert.IsNull(findDeletedItem);
        }

        [Test]
        public void FundDeleting_SubscribeAndDeleteItemThenCancelDelete_ItemNotDeleted()
        {
            var idToDeleteAndRecovery = _library.CatalogItems[1].Id;
            _library.FundDeleting += (sender, e) => { e.Cancel = true; };
            _library.DeleteItem(idToDeleteAndRecovery);
            var findDeletedAndRecoveryItem = _library.CatalogItems.Find(item => item.Id == idToDeleteAndRecovery);
            Assert.IsNotNull(findDeletedAndRecoveryItem);
        }

        [Test]
        public void SearchAndSortWithSpecifiedConditions_PriceMore10AndSortByAscendingPrice_ReturnsLibraryItemsThatPriceMore10AndWereSortedByAscending()
        {
            var price = 10;
            Func<LibraryItem, bool> conditionSearch = item => item.Price > price;
            Func<LibraryItem, decimal> conditionSort = item => item.Price;
            var result = (_library.SearchAndSortWithSpecifiedConditions(conditionSearch, SortingOrder.ByAscending, conditionSort)).ToList();
            for (var i = 1; i < result.Count; i++)
            {
                Assert.True(result.ElementAt(i).Price > price && result.ElementAt(i - 1).Price <= result.ElementAt(i).Price);
            }
        }

        [Test]
        public void SearchAndSortWithSpecifiedConditions_PriceMore15AndSortByDescendingPrice_ReturnsLibraryItemsThatPriceMore15AndWereSortedByDescending()
        {
            var price = 15;
            Func<LibraryItem, bool> conditionSearch = item => item.Price > price;
            Func<LibraryItem, decimal> conditionSort = item => item.Price;
            var result = (_library.SearchAndSortWithSpecifiedConditions(conditionSearch, SortingOrder.ByDescending, conditionSort)).ToList();
            for (var i = 1; i < result.Count; i++)
            {
                Assert.True(result.ElementAt(i).Price > price && result.ElementAt(i-1).Price >= result.ElementAt(i).Price);
            }
        }

        [Test]
        public void CreateReport__CallGenerateReport()
        {
            var fileName = "report.txt";
            var pathDirectory = Directory.GetCurrentDirectory();
            var fullPath = Path.Combine(pathDirectory, fileName);
            var mockBuilderReport = new Mock<IReportBuilder>();
            mockBuilderReport.Setup(mock => mock.GenerateReport(fullPath, _library.CatalogItems));
            _library.CreateReport(fileName, mockBuilderReport.Object);
            mockBuilderReport.Verify();
        }

        [TestCase(null)]
        [TestCase("")]
        public void CreateReport_FileNameNullOrEmpty_ThrowsArgumentException(string fileName)
        {
            Assert.Throws<ArgumentException>(() =>
            {
                _library.CreateReport(fileName, new TxtReportBuilder(new Book(), new Newspaper(), new Patent()));
            });
        }

        [Test]
        public void CreateReport_FileNameNotExistExtension_ThrowsArgumentException()
        {
            var fileName = "report";
            Assert.Throws<ArgumentException>(() =>
            {
                _library.CreateReport(fileName, new TxtReportBuilder(new Book(), new Newspaper(), new Patent()));
            });
        }

        [Test]
        public void CreateReport_FileNameExistInvalidChar_ThrowsArgumentException()
        {
            var invalidChar = Path.GetInvalidFileNameChars().First();
            var fileName = invalidChar + "report.txt";
            Assert.Throws<ArgumentException>(() =>
            {
                _library.CreateReport(fileName, new TxtReportBuilder(new Book(), new Newspaper(), new Patent()));
            });
        }

        [Test]
        public void Validate__CallIsValidItem()
        {
            var tempBook = new Book()
            {
                Name = "Пиковая дама",
                Authors = { "кто-то", "Пушкин" },
                YearOfPublication = 1834,
                NameOfThePublishingHouse = "эксмо",
                PageCount = 200,
                Note = "Примечание",
                Price = 15.5m
            };
            var mockValidator = new Mock<IValidator>();
            mockValidator.Setup(mock => mock.IsValidItem(tempBook));
            _library.Validate(tempBook, mockValidator.Object);
            mockValidator.Verify();
        }

        [TestCase(null)]
        [TestCase("")]
        public void Save_FileNameNullOrEmpty_ThrowsArgumentException(string fileName)
        {
            Assert.Throws<ArgumentException>(() =>
            {
                _library.Save(fileName, DataManager);
            });
        }

        [Test]
        public void Save_FileNameNotExistExtension_ThrowsArgumentException()
        {
            var fileName = "test";
            Assert.Throws<ArgumentException>(() =>
            {
                _library.Save(fileName, DataManager);
            });
        }

        [Test]
        public void Save___CallSaveData()
        {
            _mockDataManager.Reset();
            var fileName = "test.json";
            _mockDataManager.Setup(mock => mock.SaveData(It.IsAny<IEnumerable<LibraryItem>>(), It.IsAny<string>()));
            _library.Save(fileName, DataManager);
            _mockDataManager.Verify();
        }

        [TestCase(null)]
        [TestCase("")]
        public void Load_FileNameNullOrEmpty_ThrowsArgumentException(string fileName)
        {
            Assert.Throws<ArgumentException>(() =>
            {
                _library.Load(fileName, new DataAnnotationsValidator(), ModeLoad.WithoutInterruption, DataManager);
            });
        }

        [Test]
        public void Load_FileNameNotExistExtension_ThrowsArgumentException()
        {
            var fileName = "test";
            Assert.Throws<ArgumentException>(() =>
            {
                _library.Load(fileName, new DataAnnotationsValidator(), ModeLoad.WithoutInterruption, DataManager);
            });
        }

        [Test]
        public void Load___CallGetData()
        {
            PreparationLoad(TypeCatalogItems.NotCorrect);
            _library.Load(_fileName, new DataAnnotationsValidator(), ModeLoad.WithoutInterruption, DataManager);
            _mockDataManager.Verify();
        }

        [Test]
        public void Load_CatalogItemsNotCorrectAndModeLoadWithoutInterruption_LibraryWasLoadedAndCatalogItemsHaveBeenChanged()
        {
            var libraryForLoad = PreparationLoad(TypeCatalogItems.NotCorrect);
            _library.Load(_fileName, new DataAnnotationsValidator(), ModeLoad.WithoutInterruption, DataManager);
            Assert.AreEqual(libraryForLoad.CatalogItems.Count, _library.CatalogItems.Count);
        }

        [Test]
        public void Load_CatalogItemsNotCorrectAndModeLoadWithoutInterruption_ReturnLoadWithErrors()
        {
            PreparationLoad(TypeCatalogItems.NotCorrect);
            var result = _library.Load(_fileName, new DataAnnotationsValidator(), ModeLoad.WithoutInterruption, DataManager);
            Assert.AreEqual(ResultLoad.LoadWithErrors, result);
        }

        [Test]
        public void Load_CatalogItemsNotCorrectAndModeLoadInterruption_LibraryWasNotLoadedAndCatalogItemsHaveNotBeenChanged()
        {
            PreparationLoad(TypeCatalogItems.NotCorrect);
            var oldCountLibraryItems = _library.CatalogItems.Count;
            _library.Load(_fileName, new DataAnnotationsValidator(), ModeLoad.Interruption, DataManager);
            Assert.AreEqual(oldCountLibraryItems, _library.CatalogItems.Count);
        }

        [Test]
        public void Load_CatalogItemsNotCorrectAndModeLoadInterruption_ReturnIsNotLoad()
        {
            PreparationLoad(TypeCatalogItems.NotCorrect);
            var result = _library.Load(_fileName, new DataAnnotationsValidator(), ModeLoad.Interruption, DataManager);
            Assert.AreEqual(ResultLoad.IsNotLoad, result);
        }

        [Test]
        public void Load_CatalogItemsCorrectAndModeLoadInterruption_LibraryWasLoadedAndCatalogItemsHaveBeenChanged()
        {
            var libraryForLoad = PreparationLoad(TypeCatalogItems.Correct);
            _library.Load(_fileName, new DataAnnotationsValidator(), ModeLoad.Interruption, DataManager);
            Assert.AreEqual(libraryForLoad.CatalogItems.Count, _library.CatalogItems.Count);
        }

        [Test]
        public void Load_CatalogItemsCorrectAndModeLoadInterruption_ReturnIsLoad()
        {
            PreparationLoad(TypeCatalogItems.Correct);
            var result = _library.Load(_fileName, new DataAnnotationsValidator(), ModeLoad.Interruption, DataManager);
            Assert.AreEqual(ResultLoad.IsLoad, result);
        }

        [Test]
        public void Load_IncorrectXmlForSchema_ThrowsInvalidOperationException()
        {
            _mockDataManager.Reset();
            _mockDataManager.Setup(mock => mock.GetData(It.IsAny<string>())).Throws(new InvalidOperationException());
            Assert.Throws<InvalidOperationException>(() =>
            {
                _library.Load(_fileName, new DataAnnotationsValidator(), ModeLoad.Interruption, DataManager);
            });
        }

        [TestCase(null, "path")]
        [TestCase("", "path")]
        [TestCase("path", null)]
        [TestCase("path", "")]
        public void LoadFromXml_InputParameterOfPathNullOrEmpty_ThrowsArgumentException(string pathToXml, string pathToXslt)
        {
            Assert.Throws<ArgumentException>(() =>
            {
                _library.LoadFromXml(pathToXml, pathToXslt, DataManager);
            });
        }

        [Test]
        public void LoadFromXml_StringPathToXmlNotXmlExtension_ThrowsInvalidOperationException()
        {
            Assert.Throws<InvalidOperationException>(() =>
            {
                var fileName = "test.txt";
                _library.LoadFromXml(fileName, "path", DataManager);
            });
        }

        [Test]
        public void LoadFromXml___TransformedXmlWasCreatedThenDeleted()
        {
            var currentDirectory = Directory.GetCurrentDirectory();
            var nameXml = "ForMethodLoadFromXml.xml";
            var folderNameToXml = "XmlForTest";
            var pathToXml = Path.Combine(currentDirectory, folderNameToXml, nameXml);
            var xsltName = "ForMethodLoadFromXml.xslt";
            var folderNameToXslt = "XsltForTest";
            var pathToXslt = Path.Combine(currentDirectory, folderNameToXslt, xsltName);
            var nameTransformedXml = "Transformed" + nameXml;
            var pathToTempFolder = Path.GetTempPath();
            var pathToTransformedXml = Path.Combine(pathToTempFolder, nameTransformedXml);
            _mockDataManager.Reset();
            _mockDataManager.Setup(mock => mock.GetData(It.IsAny<string>())).Throws(new InvalidOperationException());
            Assert.Throws<InvalidOperationException>(() =>
            {
                _library.LoadFromXml(pathToXml, pathToXslt, DataManager);
            });
            FileAssert.DoesNotExist(pathToTransformedXml);
        }

        [Test]
        public void LoadFromXml___CallGetData()
        {
            var currentDirectory = Directory.GetCurrentDirectory();
            var nameXml = "ForMethodLoadFromXml.xml";
            var folderNameToXml = "XmlForTest";
            var pathToXml = Path.Combine(currentDirectory, folderNameToXml, nameXml);
            var xsltName = "ForMethodLoadFromXml.xslt";
            var folderNameToXslt = "XsltForTest";
            var pathToXslt = Path.Combine(currentDirectory, folderNameToXslt, xsltName);
            _mockDataManager.Reset();
            _mockDataManager.Setup(mock => mock.GetData(It.IsAny<string>())).Throws(new InvalidOperationException());
            Assert.Throws<InvalidOperationException>(() =>
            {
                _library.LoadFromXml(pathToXml, pathToXslt, DataManager);
            });
            _mockDataManager.Verify();
        }

        private Library PreparationLoad(TypeCatalogItems typeCatalogItems)
        {
            _mockDataManager.Reset();
            var libraryForLoad = new Library();
            if (typeCatalogItems == TypeCatalogItems.NotCorrect)
            {
                InitSomeNotCorrectItems(libraryForLoad);
            }
            else
            {
                InitAllCorrectItems(libraryForLoad);
            }
            _mockDataManager.Setup(mock => mock.GetData(It.IsAny<string>())).Returns(libraryForLoad.CatalogItems);
            return libraryForLoad;
        }

        private void InitAllCorrectItems(Library library)
        {
            var tempBook = new Book
            {
                Name = "Книга",
                Authors = { "Пушкин", "Лермонтов" },
                CityOfPublication = "Ижевск",
                NameOfThePublishingHouse = "Печать",
                YearOfPublication = 2000,
                PageCount = 50,
                Note = "примечание",
                Price = 100,
                NumberOfInstances = 5,
                Isbn = "978-1-12-123456-1"
            };
            library.AddItem(tempBook);
            var tempNewspaper = new Newspaper
            {
                Name = "Газета",
                PageCount = 50,
                Note = "примечание",
                Price = 100,
                NameOfThePublishingHouse = "Печать",
                Number = 3,
                YearOfPublication = 2000,
                NumberOfInstances = 5,
                ReleaseDate = new DateTime(2000, 7, 20),
                Issn = "1234-1234"
            };
            library.AddItem(tempNewspaper);
            var tempPatent = new Patent
            {
                Name = "Патент",
                PageCount = 50,
                Note = "примечание",
                Price = 100,
                Inventor = { "Тесла" },
                Country = "америка",
                DateOfPublication = new(2000, 7, 20),
                ApplicationSubmissionDate = new(2002, 7, 20),
                RegistrationNumber = "12345678"
            };
            library.AddItem(tempPatent);
        }

        private void InitSomeNotCorrectItems(Library library)
        {
            var tempBookCorrect = new Book
            {
                Name = "Книга",
                Authors = { "Пушкин", "Лермонтов" },
                CityOfPublication = "Ижевск",
                NameOfThePublishingHouse = "Печать",
                YearOfPublication = 2000,
                PageCount = 50,
                Note = "примечание",
                Price = 100,
                NumberOfInstances = 5,
                Isbn = "978-1-12-123456-1"
            };
            library.AddItem(tempBookCorrect);
            var tempNewspaperNotCorrect = new Newspaper()
            {
                Name = "Из рук в руки",
                CityOfPublication = "Ижевск",
                NameOfThePublishingHouse = "Арбита",
                Number = 50,
                Price = 12.5m
            };
            library.AddItem(tempNewspaperNotCorrect);
            var tempPatentNotCorrect = new Patent()
            {
                Name = "Лампочка",
                Inventor = { "Эдисон" },
                Country = "Америка",
                RegistrationNumber = "985",
                ApplicationSubmissionDate = new DateTime(2014, 7, 11),
                DateOfPublication = new DateTime(2015, 7, 20),
                Price = 17
            };
            library.AddItem(tempPatentNotCorrect);
        }
    }
}