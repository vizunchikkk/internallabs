﻿using ADO.Net.Enums;
using ADO.Net.Interfaces;
using ADO.Net.Models;
using System;
using System.Collections.Generic;

namespace ADO.Net.Services
{
    public class OrderService
    {
        private readonly IOrderRepository _orderRepository;

        public OrderService(IOrderRepository orderRepository)
        {
            _orderRepository = orderRepository;
        }

        public IEnumerable<Order> FindAllOrders()
        {
            return _orderRepository.FindAll();
        }

        public Order FindOrderById(int orderId)
        {
            var foundOrder = _orderRepository.FindById(orderId);
            if (foundOrder is null)
            {
                throw new InvalidOperationException("Заказ не найден");
            }
            return foundOrder;
        }

        public int CreateOrder(Order order)
        {
            if (order is null)
            {
                throw new ArgumentNullException(nameof(order));
            }
            return _orderRepository.Create(order);
        }

        public void UpdateOrder(Order order)
        {
            if (order is null)
            {
                throw new ArgumentNullException(nameof(order));
            }
            var foundOrder = FindOrderById(order.OrderID);
            if (foundOrder.OrderStatus == OrderStatus.InProgress || foundOrder.OrderStatus == OrderStatus.Completed)
            {
                var currentOrderStatus = foundOrder.OrderStatus.ToString();
                var errorMessage = @"Данная операция запрещена для заказов в статусе ""В процессе"" или ""Выполнен"", статус данного заказа " + currentOrderStatus;
                throw new InvalidOperationException(errorMessage);
            }
            _orderRepository.Update(order);
        }

        public void DeleteOrder(int orderId)
        {
            var foundOrder = FindOrderById(orderId);
            if (foundOrder.OrderStatus == OrderStatus.Completed)
            {
                throw new InvalidOperationException(@"Данная операция запрещена для заказов в статусе ""Выполнен""");
            }
            _orderRepository.Delete(orderId);
        }

        public IEnumerable<CustOrderHistResult> CustOrderHist(string customerId)
        {
            if (String.IsNullOrEmpty(customerId))
            {
                throw new ArgumentNullException(nameof(customerId));
            }
            return _orderRepository.CustOrderHist(customerId);
        }

        public IEnumerable<CustOrdersDetailResult> CustOrdersDetail(int orderId)
        {
            return _orderRepository.CustOrdersDetail(orderId);
        }

        public void SetOrderStatusInProgress(int orderId, DateTime orderDate)
        {
            var foundOrder = FindOrderById(orderId);
            if (foundOrder.OrderStatus != OrderStatus.New)
            {
                var currentOrderStatus = foundOrder.OrderStatus.ToString();
                var errorMessage = @"Данная операция запрещена для заказов в статусе ""В процессе"" или ""Выполнен"", статус данного заказа " + currentOrderStatus;
                throw new InvalidOperationException(errorMessage);
            }
            _orderRepository.SetStatusInProgress(orderId, orderDate);
        }

        public void SetOrderStatusCompleted(int orderId, DateTime shippedDate)
        {
            var foundOrder = FindOrderById(orderId);
            if (foundOrder.OrderStatus != OrderStatus.InProgress)
            {
                var currentOrderStatus = foundOrder.OrderStatus.ToString();
                var errorMessage = @"Данная операция запрещена для заказов в статусе ""Новый"" или ""Выполнен"", статус данного заказа " + currentOrderStatus;
                throw new InvalidOperationException(errorMessage);
            }
            _orderRepository.SetStatusCompleted(orderId, shippedDate);
        }
    }
}
