﻿using ADO.Net.Interfaces;
using ADO.Net.Models;
using ADO.Net.Services;
using Moq;
using NUnit.Framework;
using System;

namespace ADO.Net.Test.ServicesTest
{
    public class OrderServiceTest
    {
        private Mock<IOrderRepository> _mockOrderRepository = new();
        private IOrderRepository OrderRepository => _mockOrderRepository.Object;
        private OrderService _bll;

        [SetUp]
        public void Setup()
        {
            _mockOrderRepository.Reset();
            _bll = new OrderService(OrderRepository);
        }

        [Test]
        public void FindAllOrders___CallFindAll()
        {
            _mockOrderRepository.Setup(mock => mock.FindAll());
            _bll.FindAllOrders();
            _mockOrderRepository.Verify();
        }

        [Test]
        public void FindOrderById___CallFindById()
        {
            _mockOrderRepository.Setup(mock => mock.FindById(It.IsAny <int>())).Returns(new Order());
            _bll.FindOrderById(It.IsAny<int>());
            _mockOrderRepository.Verify();
        }

        [Test]
        public void FindOrderById_NotExistingId_ThrowsInvalidOperationException()
        {
            var order = new Order();
            order = null;
            _mockOrderRepository.Setup(mock => mock.FindById(It.IsAny<int>())).Returns(order); ;
            Assert.Throws<InvalidOperationException>(() =>
            {
                _bll.FindOrderById(It.IsAny<int>());
            });
        }

        [Test]
        public void CreateOrder___CallCreate()
        {
            var order = new Order();
            _mockOrderRepository.Setup(mock => mock.Create(order));
            _bll.CreateOrder(order);
            _mockOrderRepository.Verify();
        }

        [Test]
        public void CreateOrder_OrderIsNull_ThrowsArgumentNullException()
        {
            var order = new Order();
            order = null;
            Assert.Throws<ArgumentNullException>(() =>
            {
                _bll.CreateOrder(order);
            });
        }

        [Test]
        public void UpdateOrder_OrderWithStatusNew_CallUpdate()
        {
            var order = new Order();
            _mockOrderRepository.Setup(mock => mock.FindById(It.IsAny<int>())).Returns(order);
            _mockOrderRepository.Setup(mock => mock.Update(order));
            _bll.UpdateOrder(order);
            _mockOrderRepository.Verify();
        }

        [Test]
        public void UpdateOrder_OrderIsNull_ThrowsArgumentNullException()
        {
            var order = new Order();
            order = null;
            Assert.Throws<ArgumentNullException>(() =>
            {
                _bll.UpdateOrder(order);
            });
        }

        [Test]
        public void UpdateOrder_OrderWithStatusInProgress_ThrowsInvalidOperationException()
        {
            _mockOrderRepository.Setup(mock => mock.FindById(It.IsAny<int>())).Returns(new Order
            {
                OrderDate = new DateTime(2001, 1, 1)
            });
            Assert.Throws<InvalidOperationException>(() =>
            {
                _bll.UpdateOrder(new Order());
            });
        }

        [Test]
        public void UpdateOrder_OrderWithStatusCompleted_ThrowsInvalidOperationException()
        {
            _mockOrderRepository.Setup(mock => mock.FindById(It.IsAny<int>())).Returns(new Order
            {
                OrderDate = new DateTime(2001, 1, 1),
                ShippedDate = DateTime.Now
            });
            Assert.Throws<InvalidOperationException>(() =>
            {
                _bll.UpdateOrder(new Order());
            });
        }

        [Test]
        public void DeleteOrder_OrderWithStatusNew_CallDelete()
        {
            _mockOrderRepository.Setup(mock => mock.FindById(It.IsAny<int>())).Returns(new Order());
            _mockOrderRepository.Setup(mock => mock.Delete(It.IsAny<int>()));
            _bll.DeleteOrder(It.IsAny<int>());
            _mockOrderRepository.Verify();
        }

        [Test]
        public void DeleteOrder_OrderWithStatusInProgress_CallDelete()
        {
            _mockOrderRepository.Setup(mock => mock.FindById(It.IsAny<int>())).Returns(new Order
            {
                OrderDate = new DateTime(2001, 1, 1)
            });
            _mockOrderRepository.Setup(mock => mock.Delete(It.IsAny<int>()));
            _bll.DeleteOrder(It.IsAny<int>());
            _mockOrderRepository.Verify();
        }

        [Test]
        public void DeleteOrder_OrderWithStatusCompleted_ThrowsInvalidOperationException()
        {
             _mockOrderRepository.Setup(mock => mock.FindById(It.IsAny<int>())).Returns(new Order 
            {
                OrderDate = new DateTime(2001, 1, 1),
                ShippedDate = DateTime.Now
            });
            Assert.Throws<InvalidOperationException>(() =>
            {
                _bll.DeleteOrder(It.IsAny<int>());
            });
        }

        [TestCase("")]
        [TestCase(null)]
        public void CustOrderHist_CustomerIdNullOrEmpty_ThrowArgumentNullException(string customerId)
        {
            Assert.Throws<ArgumentNullException>(() =>
            {
                _bll.CustOrderHist(customerId);
            });
        }

        [Test]
        public void CustOrderHist___CallCustOrderHistInIOrderRepository()
        {
            var customerId = "a";
            _mockOrderRepository.Setup(mock => mock.CustOrderHist(customerId));
            _bll.CustOrderHist(customerId);
            _mockOrderRepository.Verify();
        }

        [Test]
        public void CustOrdersDetail___CallCustOrdersDetailInIOrderRepository()
        {
            _mockOrderRepository.Setup(mock => mock.CustOrdersDetail(It.IsAny<int>()));
            _bll.CustOrdersDetail(It.IsAny<int>());
            _mockOrderRepository.Verify();
        }

        [Test]
        public void SetOrderStatusInProgress_OrderWithStatusNew_CallSetStatusInProgress()
        {
            _mockOrderRepository.Setup(mock => mock.FindById(It.IsAny<int>())).Returns(new Order());
            _mockOrderRepository.Setup(mock => mock.SetStatusInProgress(It.IsAny<int>(), It.IsAny <DateTime>()));
            _bll.SetOrderStatusInProgress(It.IsAny<int>(), It.IsAny<DateTime>());
            _mockOrderRepository.Verify();
        }

        [Test]
        public void SetOrderStatusInProgress_OrderWithStatusCompleted_ThrowsInvalidOperationException()
        {
            _mockOrderRepository.Setup(mock => mock.FindById(It.IsAny<int>())).Returns(new Order
            {
                OrderDate = new DateTime(2001, 1, 1),
                ShippedDate = DateTime.Now
            });
            Assert.Throws<InvalidOperationException>(() =>
            {
                _bll.SetOrderStatusInProgress(It.IsAny<int>(), DateTime.Now);
            });
        }

        [Test]
        public void SetOrderStatusInProgress_OrderWithStatusInProgress_ThrowsInvalidOperationException()
        {
            _mockOrderRepository.Setup(mock => mock.FindById(It.IsAny<int>())).Returns(new Order
            {
                OrderDate = new DateTime(2001, 1, 1)
            });
            Assert.Throws<InvalidOperationException>(() =>
            {
                _bll.SetOrderStatusInProgress(It.IsAny<int>(), DateTime.Now);
            });
        }

        [Test]
        public void SetOrderStatusCompleted_OrderWithStatusInProgress_CallSetStatusCompleted()
        {
            _mockOrderRepository.Setup(mock => mock.FindById(It.IsAny<int>())).Returns(new Order
            {
                OrderDate = new DateTime(2001, 1, 1)
            });
            _mockOrderRepository.Setup(mock => mock.SetStatusCompleted(It.IsAny<int>(), It.IsAny<DateTime>()));
            _bll.SetOrderStatusCompleted(It.IsAny<int>(), It.IsAny<DateTime>());
            _mockOrderRepository.Verify();
        }

        [Test]
        public void SetOrderStatusCompleted_OrderWithStatusNew_ThrowsInvalidOperationException()
        {
            _mockOrderRepository.Setup(mock => mock.FindById(It.IsAny<int>())).Returns(new Order());
            Assert.Throws<InvalidOperationException>(() =>
            {
                _bll.SetOrderStatusCompleted(It.IsAny<int>(), DateTime.Now); ;
            });
        }

        [Test]
        public void SetOrderStatusCompleted_OrderWithStatusCompleted_ThrowsInvalidOperationException()
        {
            _mockOrderRepository.Setup(mock => mock.FindById(It.IsAny<int>())).Returns(new Order
            {
                OrderDate = new DateTime(2001, 1, 1),
                ShippedDate = DateTime.Now
            });
            Assert.Throws<InvalidOperationException>(() =>
            {
                _bll.SetOrderStatusCompleted(It.IsAny<int>(), DateTime.Now); ;
            });
        }
    }
}
