﻿using ADO.Net.Interfaces;
using ADO.Net.Mapping;
using ADO.Net.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;

namespace ADO.Net.Dal
{
    public class AdoOrderRepository : IOrderRepository
    {
        private readonly DbProviderFactory _providerFactory;
        private readonly string _connectionString;
        private readonly Mapper _mapper = new();

        public AdoOrderRepository(DbProviderFactory provider, string connectionString)
        {
            _providerFactory = provider;
            _connectionString = connectionString;
        }

        public IEnumerable<Order> FindAll()
        {
            var procedureName = "dbo.FindAllOrders";
            return ExecuteStoredProcedure(procedureName, ReadAnswerToQuery<Order>);
        }
 
        private T ExecuteStoredProcedure<T>(string commandText, Func<IDataReader, T> executeMethodForRead, IDictionary<string, object> parameters = null)
        {
            using (var dbConnection = _providerFactory.CreateConnection())
            {
                dbConnection.ConnectionString = _connectionString;
                dbConnection.Open();
                using (var command = dbConnection.CreateCommand())
                {
                    command.CommandText = commandText;
                    command.CommandType = CommandType.StoredProcedure;
                    CreateParameters(command, parameters);
                    using (var reader = command.ExecuteReader())
                    {
                        return executeMethodForRead(reader);
                    }
                }
            }
        }

        private void CreateParameters<T>(IDbCommand command, IDictionary<string, T> parameters)
        {
            if (parameters is null  || !parameters.Any())
            {
                return;
            }
            foreach (var paramPair in parameters)
            {
                var param = command.CreateParameter();
                param.ParameterName = paramPair.Key;
                param.Value = parameters[paramPair.Key];
                command.Parameters.Add(param);
            }
        }

        private IEnumerable<T> ReadAnswerToQuery<T>(IDataReader reader) where T : new()
        {
            var result = new List<T>();
            while (reader.Read())
            {
                var item = _mapper.Map<T>(reader);
                result.Add(item);
            }
            return result;
        }

        public Order FindById(int id)
        {
            var parameters = new Dictionary<string, object>
            {
                ["@OrderID"] = id
            };
            var procedureName = "dbo.FindOrderAndDetailsById";
            return ExecuteStoredProcedure(procedureName, FindOrderWithOrderDetails, parameters);
        }

        private Order FindOrderWithOrderDetails(IDataReader reader)
        {
            if (!reader.Read())
            {
                return null;
            } 
            var order = _mapper.Map<Order>(reader);
            order.OrderDetails = new List<OrderDetails>();
            reader.NextResult();
            while (reader.Read())
            {
                var orderDetails = _mapper.Map<OrderDetails>(reader);
                orderDetails.Product = _mapper.Map<Product>(reader);
                order.OrderDetails.Add(orderDetails);
            }
            return order;
        }

        public IEnumerable<CustOrderHistResult> CustOrderHist(string customerId)
        {
            var parameters = new Dictionary<string, object>
            {
                ["@CustomerID"] = customerId
            };
            var procedureName = "dbo.CustOrderHist";
            return ExecuteStoredProcedure(procedureName, ReadAnswerToQuery<CustOrderHistResult>, parameters);
        }

        public IEnumerable<CustOrdersDetailResult> CustOrdersDetail(int orderId)
        {
            var parameters = new Dictionary<string, object>
            {
                ["@OrderID"] = orderId
            };
            var procedureName = "dbo.CustOrdersDetail";
            return ExecuteStoredProcedure(procedureName, ReadAnswerToQuery<CustOrdersDetailResult>, parameters);
        }

        public int Create(Order item)
        {
            var procedureName = "dbo.CreateOrder";
            var detailsList = CreateOrderDetailsParameter(item.OrderDetails);
            using (var dbConnection = _providerFactory.CreateConnection())
            {
                dbConnection.ConnectionString = _connectionString;
                dbConnection.Open();
                using (var command = dbConnection.CreateCommand())
                {
                    command.CommandText = procedureName;
                    command.CommandType = CommandType.StoredProcedure;
                    SqlCommandBuilder.DeriveParameters((SqlCommand)command);
                    AddOrderPropertiesForCommand(command, item);
                    command.Parameters["@List"].Value = detailsList;
                    command.Parameters["@InsertOrderID"].Direction = ParameterDirection.Output;
                    command.ExecuteNonQuery();
                    return (int)command.Parameters["@InsertOrderID"].Value;
                }
            }
        }

        private DataTable CreateOrderDetailsParameter(List<OrderDetails> details)
        {
            var orderDetails = new DataTable();
            orderDetails.Columns.Add(new DataColumn("DetailProductID", typeof(int)));
            orderDetails.Columns.Add(new DataColumn("DetailUnitPrice", typeof(decimal)));
            orderDetails.Columns.Add(new DataColumn("DetailQuantity", typeof(short)));
            orderDetails.Columns.Add(new DataColumn("DetailDiscount", typeof(float)));
            foreach (var detail in details)
            {
                orderDetails.Rows.Add(detail.ProductID, detail.UnitPrice, detail.Quantity, detail.Discount);
            }
            return orderDetails;
        }

        private void AddOrderPropertiesForCommand(DbCommand command, Order item)
        {
            var properties = item.GetType().GetProperties().ToList();
            foreach (DbParameter parameter in command.Parameters)
            {
                var parameterName = parameter.ParameterName.TrimStart('@');
                if (parameter.Direction == ParameterDirection.Input)
                {
                    var property = properties.Where(prop => prop.Name.Equals(parameterName, StringComparison.OrdinalIgnoreCase)).FirstOrDefault();
                    if (property is not null)
                    {
                        var parameterValue = property.GetValue(item);
                        parameter.Value = parameterValue is null ? DBNull.Value : parameterValue;
                    }
                }
            }
        }

        public void Update(Order item)
        {
            var procedureName = "dbo.UpdateOrder";
            var detailsList = CreateOrderDetailsParameter(item.OrderDetails);
            using (var dbConnection = _providerFactory.CreateConnection())
            {
                dbConnection.ConnectionString = _connectionString;
                dbConnection.Open();
                using (var command = dbConnection.CreateCommand())
                {
                    command.CommandText = procedureName;
                    command.CommandType = CommandType.StoredProcedure;
                    SqlCommandBuilder.DeriveParameters((SqlCommand)command);
                    AddOrderPropertiesForCommand(command, item);
                    command.Parameters["@List"].Value = detailsList;
                    command.ExecuteNonQuery();
                }
            }
        }

        public void Delete(int id)
        {
            var procedureName = "dbo.DeleteOrder";
            var parameters = new Dictionary<string, object>
            {
                ["@OrderID"] = id
            };
            ExecuteNonQueryProcedure(procedureName, parameters);
        }

        private void ExecuteNonQueryProcedure(string procedureName, IDictionary<string, object> parameters)
        {
            using (var dbConnection = _providerFactory.CreateConnection())
            {
                dbConnection.ConnectionString = _connectionString;
                dbConnection.Open();
                using (var command = dbConnection.CreateCommand())
                {
                    command.CommandText = procedureName;
                    command.CommandType = CommandType.StoredProcedure;
                    CreateParameters(command, parameters);
                    command.ExecuteNonQuery();
                }
            }
        }

        public void SetStatusInProgress(int orderId, DateTime orderDate)
        {
            var parameters = new Dictionary<string, object>
            {
                ["@OrderID"] = orderId,
                ["@NewDate"] = orderDate
            };
            var procedureName = "dbo.SetOrderStatusInProgress";
            ExecuteNonQueryProcedure(procedureName, parameters);
        }

        public void SetStatusCompleted(int orderId, DateTime shippedDate)
        {
            var parameters = new Dictionary<string, object>
            {
                ["@OrderID"] = orderId,
                ["@NewDate"] = shippedDate
            };
            var procedureName = "dbo.SetOrderStatusCompleted";
            ExecuteNonQueryProcedure(procedureName, parameters);
        }
    }
}
