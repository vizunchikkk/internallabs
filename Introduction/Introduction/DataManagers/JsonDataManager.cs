﻿using Introduction.Interfaces;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;

namespace Introduction.DataManagers
{
    public class JsonDataManager<T> : IDataManager<T> where T : class
    {
        private readonly DataContractJsonSerializerSettings _dataContractJsonSerializerSettings;
        private readonly Type[] _allHeirsTType;
        private readonly string[] _allHeirsTTypeString;

        public JsonDataManager()
        {
            _allHeirsTType = AppDomain.CurrentDomain.GetAssemblies().SelectMany(assembly => assembly.GetTypes()).Where(type => type.IsSubclassOf(typeof(T))).ToArray();
            _allHeirsTTypeString = _allHeirsTType.Select(type => type.Name).ToArray();
            _dataContractJsonSerializerSettings = new DataContractJsonSerializerSettings()
            {
                DateTimeFormat = new DateTimeFormat("G"),
                KnownTypes = _allHeirsTType
            };
        }

        public IEnumerable<T> GetData(string pathToFile)
        {
            var json = new DataContractJsonSerializer(typeof(T), _dataContractJsonSerializerSettings);
            using var reader = new StreamReader(pathToFile);
            using var jsonReader = new JsonTextReader(reader);
            while (jsonReader.Read())
            {
                if (jsonReader.TokenType == JsonToken.StartObject)
                {
                    var item = JObject.Load(jsonReader);
                    if (IsReadJsonTokenTypeContainsHeirOfTypeT(item))
                    {
                        using (var stream = new MemoryStream(Encoding.UTF8.GetBytes(item.ToString())))
                        {
                            yield return (T)json.ReadObject(stream);
                        }  
                    }
                }
            }
        }

        private bool IsReadJsonTokenTypeContainsHeirOfTypeT(JObject token)
        {
            return _allHeirsTTypeString.Any(type => ((string)token["__type"]).Contains(type));
        }

        public void SaveData(IEnumerable<T> items, string pathToFile)
        {
            using var writer = new FileStream(pathToFile, FileMode.Create);
            var json = new DataContractJsonSerializer(items.GetType(), _dataContractJsonSerializerSettings);
            json.WriteObject(writer, items);
        }
    }
}
