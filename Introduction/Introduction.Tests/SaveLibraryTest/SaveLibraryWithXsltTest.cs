﻿using Introduction.DataManagers;
using Introduction.LibraryModels;
using Introduction.SaveLibrary;
using NUnit.Framework;
using System;
using System.IO;

namespace Introduction.Tests.SaveLibraryTest
{
    public class SaveLibraryWithXsltTest
    {
        private SaveLibraryWithXslt _saveLibraryWithXslt;
        private Library _library;
        private XmlDataManager<LibraryItem> _xmlConverter;
        private static readonly string _pathDirectory = Directory.GetCurrentDirectory();
        private readonly string _pathToXsd = GetPathToXmlSchemaLibrary();
        private static readonly string _folderNameToXslt = "XsltTransformers";

        [SetUp]
        public void Setup()
        {
            _saveLibraryWithXslt = new SaveLibraryWithXslt();
            _xmlConverter = new XmlDataManager<LibraryItem>(_pathToXsd);
            _library = new Library();
        }

        private void Init()
        {
            var tempBook = new Book
            {
                Name = "Книга",
                Authors = { "Пушкин", "Лермонтов" },
                CityOfPublication = "Ижевск",
                NameOfThePublishingHouse = "Печать",
                YearOfPublication = 2000,
                PageCount = 50,
                Note = "примечание",
                Price = 100,
                NumberOfInstances = 5,
                Isbn = "978-1-12-123456-1"
            };
            _library.AddItem(tempBook);
            tempBook = new Book
            {
                Name = "Книга2",
                Authors = { "Пушкин" },
                CityOfPublication = "Ижевск2",
                NameOfThePublishingHouse = "Печать2",
                YearOfPublication = 2001,
                PageCount = 500,
                Note = "примечание2",
                Price = 100,
                NumberOfInstances = 6,
                Isbn = "978-1-12-123456-1"
            };
            _library.AddItem(tempBook);
            var tempNewspaper = new Newspaper
            {
                Name = "Газета",
                PageCount = 50,
                Note = "примечание",
                Price = 100,
                NameOfThePublishingHouse = "Печать",
                Number = 3,
                YearOfPublication = 2000,
                NumberOfInstances = 5,
                ReleaseDate = new DateTime(2000, 7, 20),
                Issn = "1234-1234"
            };
            _library.AddItem(tempNewspaper);
            var tempPatent = new Patent
            {
                Name = "Патент",
                PageCount = 50,
                Note = "примечание",
                Price = 100,
                Inventor = { "Тесла" },
                Country = "америка",
                DateOfPublication = new(2000, 7, 20),
                ApplicationSubmissionDate = new(2002, 7, 20),
                RegistrationNumber = "12345678"
            };
            _library.AddItem(tempPatent);
            tempPatent = new Patent
            {
                Name = "Патент2",
                PageCount = 500,
                Note = "примечание2",
                Price = 1000,
                Inventor = { "Тесла" },
                Country = "россия",
                DateOfPublication = new(2001, 7, 20),
                ApplicationSubmissionDate = new(2003, 7, 20),
                RegistrationNumber = "12345678"
            };
            _library.AddItem(tempPatent);
        }

        private static string GetPathToXmlSchemaLibrary()
        {
            var nameXsd = "XmlSchemaLibrary.xsd";
            var folderNameToXsd = "Xsd";
            return Path.Combine(_pathDirectory, folderNameToXsd, nameXsd);
        }

        [Test]
        public void SaveLibraryInNewFormat_TransformerToRss_XmlFileHasBeenTransformed()
        {
            var serializableLibraryFileName = "ForTestTransformerToRss.xml";
            var pathToSerializableLibrary = Path.Combine(_pathDirectory, serializableLibraryFileName);
            Init();
            _xmlConverter.SaveData(_library.CatalogItems, serializableLibraryFileName);
            var xsltName = "TransformerToRss.xslt";
            var pathToXslt = Path.Combine(_pathDirectory, _folderNameToXslt, xsltName);
            var resultFileName = "ResultTransformerToRss.xml";
            var pathToOutputResult = Path.Combine(_pathDirectory, resultFileName);
            _saveLibraryWithXslt.SaveLibraryInNewFormat(pathToSerializableLibrary, pathToXslt, pathToOutputResult);
            FileAssert.Exists(pathToOutputResult);
        }

        [Test]
        public void SaveLibraryInNewFormat_TransformerToAtom_XmlFileHasBeenTransformed()
        {
            var serializableLibraryFileName = "ForTestTransformerToAtom.xml";
            var pathToSerializableLibrary = Path.Combine(_pathDirectory, serializableLibraryFileName);
            Init();
            _xmlConverter.SaveData(_library.CatalogItems, serializableLibraryFileName);
            var xsltName = "TransformerToAtom.xslt";
            var pathToXslt = Path.Combine(_pathDirectory, _folderNameToXslt, xsltName);
            var resultFileName = "ResultTransformerToAtom.xml";
            var pathToOutputResult = Path.Combine(_pathDirectory, resultFileName);
            _saveLibraryWithXslt.SaveLibraryInNewFormat(pathToSerializableLibrary, pathToXslt, pathToOutputResult);
            FileAssert.Exists(pathToOutputResult);
        }

        [Test]
        public void SaveLibraryInNewFormat_TransformerToCurrentFunds_XmlFileHasBeenTransformed()
        {
            var serializableLibraryFileName = "ForTestTransformerCurrentFunds.xml";
            var pathToSerializableLibrary = Path.Combine(_pathDirectory, serializableLibraryFileName);
            Init();
            _xmlConverter.SaveData(_library.CatalogItems, serializableLibraryFileName);
            var xsltName = "CurrentFunds.xslt";
            var pathToXslt = Path.Combine(_pathDirectory, _folderNameToXslt, xsltName);
            var resultFileName = "ResultTransformerToCurrentFunds.html";
            var pathToOutputResult = Path.Combine(_pathDirectory, resultFileName);
            _saveLibraryWithXslt.SaveLibraryInNewFormat(pathToSerializableLibrary, pathToXslt, pathToOutputResult);
            FileAssert.Exists(pathToOutputResult);
        }

        [Test]
        public void SaveLibraryInNewFormat_TransformerToAuthors_XmlFileHasBeenTransformed()
        {
            var serializableLibraryFileName = "ForTestTransformerAuthors.xml";
            var pathToSerializableLibrary = Path.Combine(_pathDirectory, serializableLibraryFileName);
            Init();
            _xmlConverter.SaveData(_library.CatalogItems, serializableLibraryFileName);
            var xsltName = "GetAuthors.xslt";
            var pathToXslt = Path.Combine(_pathDirectory, _folderNameToXslt, xsltName);
            var resultFileName = "ResultTransformerToAuthors.html";
            var pathToOutputResult = Path.Combine(_pathDirectory, resultFileName);
            _saveLibraryWithXslt.SaveLibraryInNewFormat(pathToSerializableLibrary, pathToXslt, pathToOutputResult);
            FileAssert.Exists(pathToOutputResult);
        }

        [TestCase(null, "path", "path")]
        [TestCase("", "path", "path")]
        [TestCase("path", null, "path")]
        [TestCase("path", "", "path")]
        [TestCase("path", "path", null)]
        [TestCase("path", "path", "")]
        public void SaveLibraryInNewFormat_ParameterOfPathNullOrEmpty_ThrowsArgumentException(string pathToSerializableLibrary, string pathToXslt, string pathToOutputResult)
        {
            Assert.Throws<ArgumentException>(() =>
            {
                _saveLibraryWithXslt.SaveLibraryInNewFormat(pathToSerializableLibrary, pathToXslt, pathToOutputResult);
            });
        }
    }
}
