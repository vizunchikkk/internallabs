﻿using Introduction.Enums;
using Introduction.Results;
using Introduction.Events;
using Introduction.Interfaces;
using Introduction.LibraryModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using NLog;
using System.Xml.Xsl;
using System.Xml;
using Introduction.Validators;

namespace Introduction
{
    public class Library
    {
        private List<LibraryItem> _catalogItems;
        public event EventHandler<AddItemEventArgs> FundAdded;
        public event EventHandler<DeleteItemEventArgs> FundDeleting;
        private Logger _logger = LogManager.GetCurrentClassLogger();

        public List<LibraryItem> CatalogItems
        {
            get
            {
                return _catalogItems;
            }
            private set
            {
                _catalogItems = value;
            }
        }

        public Library()
        {
            _catalogItems = new List<LibraryItem>();
        }

        public void AddItem(LibraryItem libraryItem)
        {
            CatalogItems.Add(libraryItem);
            OnFundAdded(new AddItemEventArgs
            { 
                AddItemType = libraryItem.GetType()
            });
        }

        protected virtual void OnFundAdded(AddItemEventArgs typeAddItem)
        {
            FundAdded?.Invoke(this, typeAddItem);
        }

        public void DeleteItem(Guid id)
        {
            var foundItem = CatalogItems.Find(item => item.Id == id);
            if (foundItem is null)
            {
                throw new InvalidOperationException("Не удалось найти объект для удаления");
            }
            else
            {
                var deleteEvent = new DeleteItemEventArgs
                {
                    DeletedItemId = foundItem.Id, 
                    DeletedItemName = foundItem.Name, 
                    DeletedItemType = foundItem.GetType()
                };
                OnFundDeleting(deleteEvent);
                if (!deleteEvent.Cancel)
                {
                    CatalogItems.Remove(foundItem);
                }
            }
        }

        protected virtual void OnFundDeleting(DeleteItemEventArgs typeDeletedItem)
        {
            FundDeleting?.Invoke(this, typeDeletedItem);
        }

        public void UpdateItem(LibraryItem newItem)
        {
            var foundItem = FindItemInCatalog(newItem);
            if (foundItem is null)
            {
                throw new InvalidOperationException("Не удалось найти объект для обновления полей");
            }
            else
            {
                CompareAndEditProperties(newItem, foundItem);
            }
        }

        private void CompareAndEditProperties(LibraryItem newItem, LibraryItem checkItem)
        {
            var typeCheckItem = checkItem.GetType();
            var typeNewItem = newItem.GetType();
            if (TypesAreEquals(typeCheckItem, typeNewItem))
            {
                foreach (var property in typeCheckItem.GetProperties())
                {
                    var newItemPropertyValue = property.GetValue(newItem);
                    var checkItemPropertyValue = property.GetValue(checkItem);
                    if (checkItemPropertyValue is IEnumerable<string>)
                    {
                        if (!ValueOfListTypePropertyEquals((IEnumerable<string>)newItemPropertyValue, (IEnumerable<string>)checkItemPropertyValue))
                        {
                            EditValueOfProperty(property, checkItem, newItemPropertyValue);
                        }
                    }
                    else
                    {
                        if (!Equals(newItemPropertyValue, checkItemPropertyValue))
                        {
                            EditValueOfProperty(property, checkItem, newItemPropertyValue);
                        }
                    }
                }
            }
            else
            {
                throw new InvalidOperationException("не удалось найти объект для обновления полей");
            }
        }

        private LibraryItem FindItemInCatalog(LibraryItem libraryItem)
        {
            return CatalogItems.Find(item => item.Id == libraryItem.Id);
        }

        private bool TypesAreEquals(Type firstItem, Type secondItem)
        {
            return firstItem.Equals(secondItem);
        }

        private bool ValueOfListTypePropertyEquals(IEnumerable<string> firstValue, IEnumerable<string> secondValue)
        {
            return firstValue.SequenceEqual(secondValue);
        }

        private void EditValueOfProperty(PropertyInfo property, Object objectToChange, Object newValue)
        {
            property.SetValue(objectToChange, newValue);
        }

        public IEnumerable<LibraryItem> SearchByPartOfTheName(string partOfName)
        {
            if (String.IsNullOrEmpty(partOfName))
            {
                throw new ArgumentException("Нельзя найти объект, тк не указано название для его поиска", nameof(partOfName));
            }
            return CatalogItems.Where(item => item.Name.Contains(partOfName));
        }

        public void SortingObjectsByYearOfPublication(SortingOrder sortingOrder)
        {
            if (sortingOrder == SortingOrder.ByAscending)
            {
                CatalogItems = CatalogItems.OrderBy(item => item.YearOfPublication).ToList();
            }
            else
            {
                CatalogItems = CatalogItems.OrderByDescending(item => item.YearOfPublication).ToList();
            }
        }

        public IEnumerable<Book> SearchByAuthor(string author)
        {
            if (String.IsNullOrEmpty(author))
            {
                throw new ArgumentException("Нельзя найти книгу, тк не указан автор для поиска", nameof(author));
            }
            var booksArray = BooksSearchInCatalog();
            return booksArray.Where(item => item.Authors.Contains(author));
        }

        private IEnumerable<Book> BooksSearchInCatalog()
        {
            return CatalogItems.OfType<Book>();
        }

        public IDictionary<string, List<Book>> GroupPublishingHouseBeginsWith(string startNameOfThePublishingHouse)
        {
            if (String.IsNullOrEmpty(startNameOfThePublishingHouse))
            {
                throw new ArgumentException("Нельзя найти книги, тк не указано необходимое издательство", nameof(startNameOfThePublishingHouse));
            }
            var booksArray = BooksSearchInCatalog();
            return booksArray.Where(item => item.NameOfThePublishingHouse.StartsWith(startNameOfThePublishingHouse)).
                GroupBy(item => item.NameOfThePublishingHouse).ToDictionary(group => group.Key, books => books.ToList());
        }

        public IDictionary<int, List<LibraryItem>> GroupByYearOfPublication()
        {
            return CatalogItems.GroupBy(item => item.YearOfPublication).ToDictionary(group => group.Key, items => items.ToList());
        }

        public IEnumerable<LibraryItem> SearchAndSortWithSpecifiedConditions<T>(Func<LibraryItem, bool> conditionSearch, SortingOrder sortingOrder, Func<LibraryItem, T> conditionSort)
        {
            if (sortingOrder == SortingOrder.ByAscending)
            {
                return CatalogItems.Where(conditionSearch).OrderBy(conditionSort).ToList();
            }
            else
            {
                return CatalogItems.Where(conditionSearch).OrderByDescending(conditionSort).ToList();
            }
        }

        public void CreateReport(string fileName, IReportBuilder reportBuilder)
        {
            if (String.IsNullOrEmpty(fileName))
            {
                throw new ArgumentException("Имя файла не может быть пустым", nameof(fileName));
            }
            if (String.IsNullOrEmpty(Path.GetExtension(fileName)))
            {
                throw new ArgumentException("Имя файла не содержит расширения", nameof(fileName));
            }
            if (fileName.IndexOfAny(Path.GetInvalidFileNameChars()) >= 0)
            {
                throw new ArgumentException("Имя файла содержит недопустимые символы", nameof(fileName));
            }
            var pathDirectory = Directory.GetCurrentDirectory();
            var fullPath = Path.Combine(pathDirectory, fileName);
            reportBuilder.GenerateReport(fullPath, CatalogItems);
        }

        public ResultOfValidation Validate(LibraryItem item, IValidator validator)
        {
            return validator.IsValidItem(item);
        }

        public void Save(string fileName, IDataManager<LibraryItem> dataManager)
        {
            if (String.IsNullOrEmpty(fileName))
            {
                throw new ArgumentException("Имя файла не может быть пустым", nameof(fileName));
            }
            if (String.IsNullOrEmpty(Path.GetExtension(fileName)))
            {
                throw new ArgumentException("Имя файла не содержит расширения", nameof(fileName));
            }
            var pathDirectory = Directory.GetCurrentDirectory();
            var fullPath = Path.Combine(pathDirectory, fileName);
            dataManager.SaveData(CatalogItems, fullPath);
        }

        public ResultLoad Load(string fullPath, IValidator validator, ModeLoad modeLoad, IDataManager<LibraryItem> dataManager)
        {
            if (String.IsNullOrEmpty(fullPath))
            {
                throw new ArgumentException("Путь к файлу не может быть пустым", nameof(fullPath));
            }
            if (String.IsNullOrEmpty(Path.GetExtension(fullPath)))
            {
                throw new ArgumentException("Имя файла не содержит расширения", nameof(fullPath));
            }
            var deserializeCatalogItems = new List<LibraryItem>();
            bool haveErrors = false;
            try
            {
                foreach (var item in dataManager.GetData(fullPath))
                {
                    var valid = Validate(item, validator);
                    if (!valid.IsValid)
                    {
                        haveErrors = true;
                        if (modeLoad == ModeLoad.WithoutInterruption)
                        {
                            deserializeCatalogItems.Add(item);
                            LoggingErrorInNotValidItem(item, valid);
                        }
                        else
                        {
                            deserializeCatalogItems.Clear();
                            return ResultLoad.IsNotLoad;
                        }
                    }
                    else
                    {
                        deserializeCatalogItems.Add(item);
                    }
                }
                if (deserializeCatalogItems.Any())
                {
                    CatalogItems = deserializeCatalogItems;
                    if (haveErrors)
                    {
                        return ResultLoad.LoadWithErrors;
                    }
                    return ResultLoad.IsLoad;
                }
            }
            catch (InvalidOperationException)
            {
                throw;
            }
            return ResultLoad.IsNotLoad;
        }

        private void LoggingErrorInNotValidItem(LibraryItem item, ResultOfValidation valid)
        {
            var itemId = item.Id;
            var errors = String.Join(", ", valid.ErrorMessages);
            _logger.Info($"Id={itemId} Ошибки:{errors}");
        }

        public void LoadFromXml(string pathToXml, string pathToXslt, IDataManager<LibraryItem> dataManager)
        {
            if (String.IsNullOrEmpty(pathToXml))
            {
                throw new ArgumentException("Путь к xml файлу не может быть пустым", nameof(pathToXml));
            }
            if (String.IsNullOrEmpty(pathToXslt))
            {
                throw new ArgumentException("Путь к xslt преобразованию не может быть пустым", nameof(pathToXslt));
            }
            if (Path.GetExtension(pathToXml) != ".xml")
            {
                throw new InvalidOperationException("Данный метод работает только с xml");
            }
            var pathToTempFolder = Path.GetTempPath();
            var nameTransformedXml = "Transformed" + Path.GetFileName(pathToXml);
            var pathForTransformedXml = Path.Combine(pathToTempFolder, nameTransformedXml);
            var xslt = new XslCompiledTransform();
            xslt.Load(pathToXslt);
            var settings = new XmlWriterSettings
            {
                ConformanceLevel = ConformanceLevel.Fragment,
                Indent = true
            };
            try
            {
                using (var writer = XmlWriter.Create(new FileStream(pathForTransformedXml, FileMode.Create, FileAccess.Write, FileShare.Delete), settings))
                {
                    xslt.Transform(pathToXml, writer);
                }
                Load(pathForTransformedXml, new DataAnnotationsValidator(), ModeLoad.Interruption, dataManager);
            }
            finally
            {
                if (File.Exists(pathForTransformedXml))
                {
                    File.Delete(pathForTransformedXml);
                }
            }
        }
    }
}