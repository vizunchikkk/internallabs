﻿<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
  xml:space="preserve">
<xsl:output method="xml" version="1.0" indent="yes" omit-xml-declaration="no" standalone="yes" encoding="utf-8"/>
  
<xsl:template match="/ArrayOfLibraryItem">
<rdf:RDF xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <channel rdf:about="http://www.xml.com/xml/news.rss">
    <title>Library Item</title>
    <link>https://libItem.com/</link>
    <description>List of some library item</description>
  </channel>
  <xsl:apply-templates />
  </rdf:RDF>
</xsl:template>

  <xsl:template match="/ArrayOfLibraryItem/LibraryItem">
    <item>
      <title><xsl:value-of select="Name"/></title>
      <link>libItem.com/</link>
      <description><xsl:value-of select="Note"/></description>
    </item>
  </xsl:template>

  <xsl:template match="text() | @*"/>
</xsl:stylesheet>
