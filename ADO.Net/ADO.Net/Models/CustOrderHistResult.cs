﻿namespace ADO.Net.Models
{
    public class CustOrderHistResult
    {
        public string ProductName { get; set; }
        public int? Total { get; set; }
    }
}
