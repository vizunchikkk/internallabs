﻿using ADO.Net.Dal;

namespace ADO.Net.Test.DalTest
{
    public class DapperOrderRepositoryTest : OrderRepositoriesTest
    {
        public DapperOrderRepositoryTest()
        {
            Repository = new DapperOrderRepository(_providerFactory, _connectionString);
        }
    }
}
