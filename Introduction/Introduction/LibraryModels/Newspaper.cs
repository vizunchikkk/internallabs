﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Introduction.LibraryModels
{
    [DataContract]
    public class Newspaper : LibraryItem
    {
        [DataMember]
        public string CityOfPublication { get; set; }
        [Required(ErrorMessage = "Название издательства не может быть пустым")]
        [DataMember]
        public string NameOfThePublishingHouse { get; set; }
        [DataMember]
        [Range(1, int.MaxValue, ErrorMessage = "Номер газеты не может быть отрицательным")]
        public int Number { get; set; }
        [DataMember]
        [Range(typeof(DateTime), "1/1/0001 12:00:01 AM", "01/01/2100", ErrorMessage = "Дата издания не может быть пустой")]
        public DateTime ReleaseDate { get; set; }
        [DataMember]
        [RegularExpression(@"^\d{4}-\d{3}(\d|X)$", ErrorMessage = "ISSN должн иметь длинну 8, формат 4цифры-4цифры(последний символ может быть X)")]
        public string Issn { get; set; }
        [DataMember]
        [Range(1, int.MaxValue, ErrorMessage = "Количество экземпляров не может быть отрицательно")]
        public int NumberOfInstances { get; set; }

        public Newspaper()
        {}
    }
}
