﻿using ADO.Net.Interfaces;
using ADO.Net.Models;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;

namespace ADO.Net.Dal
{
    public class DapperOrderRepository : IOrderRepository
    {
        private readonly DbProviderFactory _providerFactory;
        private readonly string _connectionString;

        public DapperOrderRepository(DbProviderFactory provider, string connectionString)
        {
            _providerFactory = provider;
            _connectionString = connectionString;
        }

        public IEnumerable<Order> FindAll()
        {
            var orders = new List<Order>();
            var procedureName = "dbo.FindAllOrders";
            using (var dbConnection = _providerFactory.CreateConnection())
            {
                dbConnection.ConnectionString = _connectionString;
                dbConnection.Open();
                orders = dbConnection.Query<Order>(procedureName, commandType: CommandType.StoredProcedure).ToList();
            }
            return orders;
        }

        public Order FindById(int id)
        {
            Order order = null;
            var procedureNameToFindOrder = "dbo.FindOrderById";
            var procedureNameToFindOrderDetails = "dbo.FindOrderDetailsById";
            using (var dbConnection = _providerFactory.CreateConnection())
            {
                dbConnection.ConnectionString = _connectionString;
                dbConnection.Open();
                order = dbConnection.Query<Order>(procedureNameToFindOrder, new { OrderID = id }, commandType: CommandType.StoredProcedure).FirstOrDefault();
                if (order is not null)
                {
                    order.OrderDetails = dbConnection.Query<OrderDetails, Product, OrderDetails>(procedureNameToFindOrderDetails,
                    (details, prod) =>
                    {
                        details.Product = prod;
                        return details;
                    }, new { OrderID = id }, splitOn: "Discount", commandType: CommandType.StoredProcedure).ToList();
                }
                else
                {
                    return null;
                }
            }
            return order;
        }

        public int Create(Order item)
        {
            var detailsList = CreateOrderDetailsParameter(item.OrderDetails);
            var parameters = AddOrderPropertiesInDynamicParameters(item);
            parameters.Add("@List", detailsList);
            parameters.Add("@InsertOrderId", dbType: DbType.Int32, direction: ParameterDirection.Output);
            SqlMapper.AddTypeMap(typeof(DataTable), DbType.Object);
            var procedureName = "dbo.CreateOrder";
            using (var dbConnection = _providerFactory.CreateConnection())
            {
                dbConnection.ConnectionString = _connectionString;
                dbConnection.Open();
                dbConnection.Execute(procedureName, parameters , commandType: CommandType.StoredProcedure);
                return parameters.Get<int>("@InsertOrderId");
            }
        }

        private DynamicParameters AddOrderPropertiesInDynamicParameters(Order item)
        {
            var parameters = new DynamicParameters();
            parameters.Add("@CustomerID", item.CustomerID);
            parameters.Add("@EmployeeID", item.EmployeeID);
            parameters.Add("@RequiredDate", item.RequiredDate);
            parameters.Add("@ShipVia", item.ShipVia);
            parameters.Add("@Freight", item.Freight);
            parameters.Add("@ShipName", item.ShipName);
            parameters.Add("@ShipAddress", item.ShipAddress);
            parameters.Add("@ShipCity", item.ShipCity);
            parameters.Add("@ShipRegion", item.ShipRegion);
            parameters.Add("@ShipPostalCode", item.ShipPostalCode);
            parameters.Add("@ShipCountry", item.ShipCountry);
            return parameters;
        }

        private DataTable CreateOrderDetailsParameter(List<OrderDetails> details)
        {
            var orderDetails = new DataTable();
            orderDetails.Columns.Add(new DataColumn("DetailProductID", typeof(int)));
            orderDetails.Columns.Add(new DataColumn("DetailUnitPrice", typeof(decimal)));
            orderDetails.Columns.Add(new DataColumn("DetailQuantity", typeof(short)));
            orderDetails.Columns.Add(new DataColumn("DetailDiscount", typeof(float)));
            foreach (var detail in details)
            {
                orderDetails.Rows.Add(detail.ProductID, detail.UnitPrice, detail.Quantity, detail.Discount);
            }
            return orderDetails;
        }

        public IEnumerable<CustOrdersDetailResult> CustOrdersDetail(int orderId)
        {
            var custOrdersDetail = new List<CustOrdersDetailResult>();
            var procedureName = "dbo.CustOrdersDetail";
            using (var dbConnection = _providerFactory.CreateConnection())
            {
                dbConnection.ConnectionString = _connectionString;
                dbConnection.Open();
                custOrdersDetail = dbConnection.Query<CustOrdersDetailResult>(procedureName, new { orderId }, commandType: CommandType.StoredProcedure).ToList();
            }
            return custOrdersDetail;
        }

        public IEnumerable<CustOrderHistResult> CustOrderHist(string customerId)
        {
            var custOrderHist = new List<CustOrderHistResult>();
            var procedureName = "dbo.CustOrderHist";
            using (var dbConnection = _providerFactory.CreateConnection())
            {
                dbConnection.ConnectionString = _connectionString;
                dbConnection.Open();
                custOrderHist = dbConnection.Query<CustOrderHistResult>(procedureName, new { customerId }, commandType: CommandType.StoredProcedure).ToList();
            }
            return custOrderHist;
        }

        public void Delete(int id)
        {
            var procedureName = "dbo.DeleteOrder";
            using (var dbConnection = _providerFactory.CreateConnection())
            {
                dbConnection.ConnectionString = _connectionString;
                dbConnection.Open();
                dbConnection.Execute(procedureName, new { OrderID = id }, commandType: CommandType.StoredProcedure);
            }
        }

        public void SetStatusCompleted(int orderId, DateTime shippedDate)
        {
            var procedureName = "dbo.SetOrderStatusCompleted";
            using (var dbConnection = _providerFactory.CreateConnection())
            {
                dbConnection.ConnectionString = _connectionString;
                dbConnection.Open();
                dbConnection.Execute(procedureName, new { OrderID = orderId, NewDate = shippedDate }, commandType: CommandType.StoredProcedure);
            }
        }

        public void SetStatusInProgress(int orderId, DateTime orderDate)
        {
            var procedureName = "dbo.SetOrderStatusInProgress";
            using (var dbConnection = _providerFactory.CreateConnection())
            {
                dbConnection.ConnectionString = _connectionString;
                dbConnection.Open();
                dbConnection.Execute(procedureName, new { OrderID = orderId, NewDate = orderDate }, commandType: CommandType.StoredProcedure);
            }
        }

        public void Update(Order item)
        {
            var detailsList = CreateOrderDetailsParameter(item.OrderDetails);
            var parameters = AddOrderPropertiesInDynamicParameters(item);
            parameters.Add("@OrderID", item.OrderID);
            parameters.Add("@List", detailsList);
            SqlMapper.AddTypeMap(typeof(DataTable), DbType.Object);
            var procedureName = "dbo.UpdateOrder";
            using (var dbConnection = _providerFactory.CreateConnection())
            {
                dbConnection.ConnectionString = _connectionString;
                dbConnection.Open();
                dbConnection.Execute(procedureName, parameters, commandType: CommandType.StoredProcedure);
            }
        }
    }
}
