﻿using FluentValidation;
using Introduction.LibraryModels;

namespace Introduction.Validators
{
    public class FluentLibrariItemValidator : AbstractValidator<LibraryItem>
    {
        public FluentLibrariItemValidator()
        {
            RuleFor(item => item.Name).NotEmpty();
            RuleFor(item => item.PageCount).GreaterThanOrEqualTo(1);
            RuleFor(item => item.Note.Length).LessThanOrEqualTo(500);
            RuleFor(item => item.YearOfPublication).GreaterThanOrEqualTo(1900);
            RuleFor(item => item.Price).NotEmpty().GreaterThan(0);
        }
    }
}
