﻿using System;
using System.Collections.Generic;
using NUnit.Framework;
using FluentValidation.TestHelper;
using Introduction.Validators;
using Introduction.LibraryModels;

namespace Introduction.Tests.ValidatorsTest
{
    public class FluentPatentValidatorTest
    {
        private Patent _patentCorrect;
        private FluentPatentValidator _patentValidator;
        private readonly DateTime _dateControl = new(1950, 1, 1);

        [SetUp]
        public void Setup()
        {
            _patentValidator = new FluentPatentValidator();
            _patentCorrect = new Patent
            {
                Name = "Патент",
                PageCount = 50,
                Note = "примечание",
                Price = 100,
                Inventor = { "Тесла" },
                Country = "америка",
                DateOfPublication = new (2000, 7, 20),
                ApplicationSubmissionDate = new (2002, 7, 20),
                RegistrationNumber = "1-2000/1"
            };
        }

        [Test]
        public void Validate_CorrectPatentProperty_PatentIsValid()
        {
            var result = _patentValidator.TestValidate(_patentCorrect);
            Assert.True(result.IsValid);
        }

        [Test]
        public void Validate_InventorEmpty_ReturnError()
        {
            _patentCorrect.Inventor = new List<string>();
            var result = _patentValidator.TestValidate(_patentCorrect);
            result.ShouldHaveValidationErrorFor(patent => patent.Inventor);
        }

        [Test]
        public void Validate_InventorCount1_ReturnNotError()
        {
            _patentCorrect.Inventor = new List<string> { "a" };
            var result = _patentValidator.TestValidate(_patentCorrect);
            result.ShouldNotHaveValidationErrorFor(patent => patent.Inventor);
        }

        [TestCase(null)]
        [TestCase("")]
        public void Validate_CountryNullOrEmpty_ReturnError(string country)
        {
            _patentCorrect.Country = country;
            var result = _patentValidator.TestValidate(_patentCorrect);
            result.ShouldHaveValidationErrorFor(patent => patent.Country);
        }

        [Test]
        public void Validate_DateOfPublicationLessDateControl_ReturnError()
        {
            _patentCorrect.DateOfPublication = _dateControl.AddSeconds(-1);
            var result = _patentValidator.TestValidate(_patentCorrect);
            result.ShouldHaveValidationErrorFor(patent => patent.DateOfPublication);
        }

        [Test]
        public void Validate_DateOfPublicationEqualDateControl_ReturnNotError()
        {
            _patentCorrect.DateOfPublication = _dateControl;
            var result = _patentValidator.TestValidate(_patentCorrect);
            result.ShouldNotHaveValidationErrorFor(patent => patent.DateOfPublication);
        }

        [Test]
        public void Validate_ApplicationSubmissionDateLessDateControl_ReturnError()
        {
            _patentCorrect.ApplicationSubmissionDate = _dateControl.AddSeconds(-1);
            var result = _patentValidator.TestValidate(_patentCorrect);
            result.ShouldHaveValidationErrorFor(patent => patent.ApplicationSubmissionDate);
        }

        [Test]
        public void Validate_ApplicationSubmissionDateEqualDateControl_ReturnNotError()
        {
            _patentCorrect.ApplicationSubmissionDate = _dateControl;
            var result = _patentValidator.TestValidate(_patentCorrect);
            result.ShouldNotHaveValidationErrorFor(patent => patent.ApplicationSubmissionDate);
        }

        [TestCase("RE123456")]
        [TestCase("PP123456")]
        [TestCase("AI123456")]
        [TestCase("D1234567")]
        [TestCase("X1234567")]
        [TestCase("H1234567")]
        [TestCase("T1234567")]
        [TestCase("11111111")]
        public void Validate_RegistrationNumberInternationalStandardIsCorrect_ReturnNotError(string registrationNumber)
        {
            _patentCorrect.RegistrationNumber = registrationNumber;
            var result = _patentValidator.TestValidate(_patentCorrect);
            result.ShouldNotHaveValidationErrorFor(patent => patent.RegistrationNumber);
        }

        [TestCase("RE12345")]
        [TestCase("PP1234567")]
        [TestCase("D123456")]
        [TestCase("X12345678")]
        [TestCase("1234567")]
        [TestCase("123456789")]
        public void Validate_RegistrationNumberInternationalStandardNumberOfDigitsNotSupplementLengthTo8_ReturnError(string registrationNumber)
        {
            _patentCorrect.RegistrationNumber = registrationNumber;
            var result = _patentValidator.TestValidate(_patentCorrect);
            result.ShouldHaveValidationErrorFor(patent => patent.RegistrationNumber);
        }

        [TestCase("re123456")]
        [TestCase("Pp123456")]
        [TestCase("aI123456")]
        [TestCase("d1234567")]
        [TestCase("A1234567")]
        [TestCase("HI123456")]
        public void Validate_RegistrationNumberInternationalStandardInvalidLetterPrefixRegisterOrNotEqualToAnyOf7PossiblePrefixes_ReturnError(string registrationNumber)
        {
            _patentCorrect.RegistrationNumber = registrationNumber;
            var result = _patentValidator.TestValidate(_patentCorrect);
            result.ShouldHaveValidationErrorFor(patent => patent.RegistrationNumber);
        }

        [TestCase("-2000/1")]
        [TestCase("a-2000/1")]
        [TestCase("1a-2000/1")]
        [TestCase("12345678910-2000/1")]
        public void Validate_RegistrationNumberNotInternationalStandardInvalidFirstPartOfThemItIsNumber_ReturnError(string registrationNumber)
        {
            _patentCorrect.RegistrationNumber = registrationNumber;
            var result = _patentValidator.TestValidate(_patentCorrect);
            result.ShouldHaveValidationErrorFor(patent => patent.RegistrationNumber);
        }

        [TestCase("1-2000/")]
        [TestCase("1-2000/a")]
        [TestCase("1-2000/1a")]
        [TestCase("1-2000/12345678910")]
        public void Validate_RegistrationNumberNotInternationalStandardInvalidLastPartOfThemItIsVersion_ReturnError(string registrationNumber)
        {
            _patentCorrect.RegistrationNumber = registrationNumber;
            var result = _patentValidator.TestValidate(_patentCorrect);
            result.ShouldHaveValidationErrorFor(patent => patent.RegistrationNumber);
        }

        [TestCase("1-200/1")]
        [TestCase("1-200a/1")]
        [TestCase("1-a/1")]
        [TestCase("1-20001/1")]
        public void Validate_RegistrationNumberNotInternationalStandardInvalidMiddlePartOfThemItIsYear_ReturnError(string registrationNumber)
        {
            _patentCorrect.RegistrationNumber = registrationNumber;
            var result = _patentValidator.TestValidate(_patentCorrect);
            result.ShouldHaveValidationErrorFor(patent => patent.RegistrationNumber);
        }

        [TestCase("12000/1")]
        [TestCase("1-2000.1")]
        public void Validate_RegistrationNumberNotInternationalStandardInvalidSeparationSymbols_ReturnError(string registrationNumber)
        {
            _patentCorrect.RegistrationNumber = registrationNumber;
            var result = _patentValidator.TestValidate(_patentCorrect);
            result.ShouldHaveValidationErrorFor(patent => patent.RegistrationNumber);
        }

        [TestCase("1-2000/1")]
        [TestCase("1234567891-2000/1")]
        [TestCase("1234567891-2000/1234567891")]
        [TestCase("123456-0000/12345")]
        public void Validate_RegistrationNumberNotInternationalStandardCorrect_ReturnNotError(string registrationNumber)
        {
            _patentCorrect.RegistrationNumber = registrationNumber;
            var result = _patentValidator.TestValidate(_patentCorrect);
            result.ShouldNotHaveValidationErrorFor(patent => patent.RegistrationNumber);
        }
    }
}
