﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Introduction.LibraryModels
{
    [DataContract]
    public abstract class LibraryItem
    {
        [DataMember]
        public Guid Id { get; init; }
        [DataMember]
        [Required(ErrorMessage = "Название не может быть пустым")]
        public string Name { get; set; }
        [DataMember]
        [Range(1,int.MaxValue, ErrorMessage = "Количество страниц должно быть больше или равно 1")]
        public int PageCount { get; set; }
        [DataMember]
        [MaxLength(500, ErrorMessage = "Максимальный размер примечания не должен превышать 500 символов")]
        public string Note { get; set; }
        [DataMember]
        [Range(1900, int.MaxValue, ErrorMessage = "Год издания книги не может быть меньше 1900 года")]
        public int YearOfPublication { get; set; }
        [DataMember]
        [Required]
        [Range(0.01, Double.PositiveInfinity, ErrorMessage = "Стоимость не может быть пустой и отрицательной")]
        public decimal Price { get; set; }

        protected LibraryItem() 
        {
            Id = Guid.NewGuid();
        }
    }
}
