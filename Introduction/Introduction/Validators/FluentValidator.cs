﻿using Introduction.LibraryModels;
using System.Linq;
using Introduction.Results;
using System.Collections.Generic;
using System;
using FluentValidation;

namespace Introduction.Validators
{
    public class FluentValidator : Interfaces.IValidator
    {
        private Dictionary<Type, Func<LibraryItem,FluentValidation.Results.ValidationResult>> _itemValidators = new();

        public FluentValidator(IValidator<Book> validatorBook, IValidator<Newspaper> validatorNewspaper, IValidator<Patent> validatorPatent)
        {
            _itemValidators[typeof(Book)] = item => validatorBook.Validate((Book)item);
            _itemValidators[typeof(Newspaper)] = item => validatorNewspaper.Validate((Newspaper)item);
            _itemValidators[typeof(Patent)] = item => validatorPatent.Validate((Patent)item);
        }

        public ResultOfValidation IsValidItem(LibraryItem item)
        {
            FluentValidation.Results.ValidationResult validationResult = new();
            var itemType= item.GetType();
            var resultOfValidation = new ResultOfValidation();
            Func<LibraryItem, FluentValidation.Results.ValidationResult> validator = null;
            if (_itemValidators.TryGetValue(itemType, out validator))
            {
                validationResult = validator(item);
                resultOfValidation.ErrorMessages = validationResult.Errors.Select(result => result.ErrorMessage).ToList();
                resultOfValidation.IsValid = validationResult.IsValid;
            }
            else
            {
                throw new InvalidOperationException("Не удалось найти нужный тип валидатора");
            }
            return resultOfValidation;
        }
    }
}
