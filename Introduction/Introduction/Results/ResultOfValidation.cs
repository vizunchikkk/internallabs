﻿using System.Collections.Generic;

namespace Introduction.Results
{
    public class ResultOfValidation
    {
        public List<string> ErrorMessages { get; set; }
        public bool IsValid { get; set; }
    }
}
