<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:i="http://www.w3.org/2001/XMLSchema-instance">
  
<xsl:output method="xml" version="1.0" indent="yes" omit-xml-declaration="no" encoding="utf-8" standalone="yes"/>
 <xsl:param name="dateTime"/>

  <xsl:template match="ArrayOfLibraryItem">
    <feed xmlns="http://www.w3.org/2005/Atom">
      <title>Library Item</title>
      <subtitle>About Library Item</subtitle>
      <link>libItem.com/</link>
      <updated><xsl:value-of select="$dateTime"/></updated>
      <xsl:for-each select="LibraryItem">
        <entry>
          <title><xsl:value-of select="Name"/></title>
          <link>libItem.com/</link>
          <summary><xsl:value-of select="Note"/></summary>
          <author>
            <name>
              <xsl:variable name="Author_Count" select="count(Authors/*)"/>
              <xsl:variable name="Inventor_Count" select="count(Inventor/*)"/>
              <xsl:if test="($Author_Count) > 0">
                <xsl:for-each select="Authors/*"><xsl:value-of select="."/>;</xsl:for-each>
              </xsl:if>
              <xsl:if test="($Inventor_Count) > 0">
                <xsl:for-each select="Inventor/*"><xsl:value-of select="."/>;</xsl:for-each>
              </xsl:if>
              <xsl:if test="($Author_Count + $Inventor_Count) = 0">All employees</xsl:if>
            </name>
            <email>library@example.com</email>
          </author>
          <id>urn:uuid:<xsl:value-of select="Id"/></id>
          <updated><xsl:value-of select="$dateTime"/></updated>
        </entry>
      </xsl:for-each>
    </feed>
  </xsl:template>
  <xsl:template match="text() | @*"/>
</xsl:stylesheet>