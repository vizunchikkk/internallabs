﻿using System;
using System.Data;
using System.Linq;
using System.Reflection;

namespace ADO.Net.Mapping
{
    public class Mapper 
    {
        public T Map<T>(IDataReader reader) where T : new()
        {
            var obj = new T();
            var columnNames = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();
            var properties= typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (var property in properties)
            {
                if (!columnNames.Any(name => name.Equals(property.Name, StringComparison.OrdinalIgnoreCase)))
                {
                    continue;
                }
                var propertyValue = reader[property.Name];
                if (propertyValue is DBNull)
                {
                    propertyValue = null;
                }
                property.SetValue(obj, propertyValue);
            }
            return obj;
        }
    }
}
