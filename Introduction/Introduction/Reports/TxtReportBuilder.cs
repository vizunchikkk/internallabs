﻿using Introduction.Interfaces;
using Introduction.LibraryModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Introduction.Reports
{
    public class TxtReportBuilder : IReportBuilder
    {
        private Dictionary<Type, Func<LibraryItem, string>> _itemPropertiesStrings = new();

        public TxtReportBuilder(Book book, Newspaper newspaper, Patent patent)
        {
            _itemPropertiesStrings[typeof(Book)] = item => BookPropertiesStrings((Book)item);
            _itemPropertiesStrings[typeof(Newspaper)] = item => NewspaperPropertiesStrings((Newspaper)item);
            _itemPropertiesStrings[typeof(Patent)] = item => PatentPropertiesStrings((Patent)item);
        }

        public void GenerateReport(string pathToReport, List<LibraryItem> catalogForRecord)
        {
            if (!catalogForRecord.Any())
            {
                throw new ArgumentException("Библиотека объектов для записи не может буть пустой", nameof(catalogForRecord));
            }
            if (Path.GetExtension(pathToReport) != ".txt")
            {
                throw new ArgumentException("Расширение файла должно быть txt", nameof(pathToReport));
            }
            try
            {
                using (var writer = new StreamWriter(pathToReport, false, Encoding.Default))
                {
                    var bookItems = catalogForRecord.OfType<Book>();
                    WriteItems(writer, bookItems, "Книги");

                    var newspaperItems = catalogForRecord.OfType<Newspaper>();
                    WriteItems(writer, newspaperItems, "Газеты");

                    var patentItems = catalogForRecord.OfType<Patent>();
                    WriteItems(writer, patentItems, "Патенты");
                }
            }
            catch (UnauthorizedAccessException e)
            {
                throw new ArgumentException("Нельзя записывать в файл по переданному в метод пути, нет разрешения", e);
            }
            catch (DirectoryNotFoundException e)
            {
                throw new ArgumentException("Не верный путь к файлу, такой директории нет", e);
            }
            catch (PathTooLongException e)
            {
                throw new ArgumentException("Путь к файлу слишком длинный", e);
            }
        }

        private void WriteItems(StreamWriter writer, IEnumerable<LibraryItem> items, string nameType)
        {
            writer.WriteLine(nameType);
            var itemsType = items.GetType().GetGenericArguments().First();
            foreach (var item in items)
            {
                Func<LibraryItem, string> properties = null;
                if (_itemPropertiesStrings.TryGetValue(itemsType, out properties))
                {
                    var stringProperties = properties(item);
                    writer.WriteLine(stringProperties);
                }
                else
                {
                    throw new InvalidOperationException("Не удалось найти нужный тип ключа для получения значения");
                }
            }
            WriteNumberOfObjectsAndEmptyString(writer, items.Count());
        }

        private string BookPropertiesStrings(Book book)
        {
            var stringAuthors = String.Join(", ", book.Authors.ToArray());
            return String.Format("{0,-20} {1, 5}", stringAuthors, book.Name);
        }

        private string NewspaperPropertiesStrings(Newspaper newspaper)
        {
            return String.Format("{0,-20} {1, 5}", newspaper.Name, newspaper.ReleaseDate.ToString("d"));
        }

        private string PatentPropertiesStrings(Patent patent)
        {
            return String.Format("{0,-20} {1, 5}", patent.Name, patent.DateOfPublication.ToString("d"));
        }

        private void WriteNumberOfObjectsAndEmptyString (StreamWriter writer, int count)
        {
            writer.WriteLine(count);
            writer.WriteLine();
        }
    }
}
