﻿using Introduction.Validators;
using Introduction.LibraryModels;
using NUnit.Framework;
using System;
using Moq;

namespace Introduction.Tests.ValidatorsTest
{
    public class FluentValidatorTest
    {
        private FluentValidator _validator;

        [SetUp]
        public void Setup()
        {
            _validator = new(new FluentBookValidator(), new FluentNewspaperValidator(), new FluentPatentValidator());
        }

        [Test]
        public void IsValidItem_NotCorrectBook_NotEmptyListErrorsAndNotValid()
        {
            var tempBook = new Book()
            {
                Name = "Пиковая дама",
                Authors = { "кто-то", "Пушкин" },
                YearOfPublication = 1834,
                NameOfThePublishingHouse = "эксмо",
                PageCount = 200,
                Note = "Примечание",
                Price = 15.5m
            };
            var result = _validator.IsValidItem(tempBook);
            Assert.IsNotEmpty(result.ErrorMessages);
            Assert.False(result.IsValid);
        }

        [Test]
        public void IsValidItem_CorrectNewspaper_EmptyListErrorsAndValid()
        {
            var tempNewspaper = new Newspaper
            {
                Name = "Газета",
                PageCount = 50,
                Note = "примечание",
                Price = 100,
                NameOfThePublishingHouse = "Печать",
                Number = 3,
                YearOfPublication = 2000,
                NumberOfInstances = 5,
                ReleaseDate = new DateTime(2000, 7, 20)
            };
            var result = _validator.IsValidItem(tempNewspaper);
            Assert.IsEmpty(result.ErrorMessages);
            Assert.True(result.IsValid);
        }

        [Test]
        public void IsValidItem_CorrectPatent_EmptyListErrorsAndValid()
        {
            var tempPatent = new Patent
            {
                Name = "Патент",
                PageCount = 50,
                Note = "примечание",
                Price = 100,
                Inventor = { "Тесла" },
                Country = "америка",
                DateOfPublication = new(2000, 7, 20),
                ApplicationSubmissionDate = new(2002, 7, 20)
            };
            var result = _validator.IsValidItem(tempPatent);
            Assert.IsEmpty(result.ErrorMessages);
            Assert.True(result.IsValid);
        }

        [Test]
        public void IsValidItem_NotExistingTypeValidator_ThrowsInvalidOperationException()
        {
            Assert.Throws<InvalidOperationException>(() =>
            {
                var mock = new Mock<LibraryItem>();
                var libraryItem = mock.Object;
                _validator.IsValidItem(libraryItem);
            });
        }
    }
}
