﻿using NUnit.Framework;
using FluentValidation.TestHelper;
using Introduction.Validators;
using Introduction.LibraryModels;
using System;

namespace Introduction.Tests.ValidatorsTest
{
    public class FluentNewspaperValidatorTest
    {
        private Newspaper _newspaperCorrect;
        private FluentNewspaperValidator _newspaperValidator;

        [SetUp]
        public void Setup()
        {
            _newspaperValidator = new FluentNewspaperValidator();
            _newspaperCorrect = new Newspaper
            {
                Name = "Газета",
                PageCount = 50,
                Note = "примечание",
                Price = 100,
                NameOfThePublishingHouse = "Печать",
                Number = 3,
                YearOfPublication = 2000,
                NumberOfInstances = 5,
                ReleaseDate = new DateTime(2000, 7, 20),
                Issn = "1234-1234"
            };
        }

        [Test]
        public void Validate_CorrectNewspaperProperty_NewspaperIsValid()
        {
            var result = _newspaperValidator.TestValidate(_newspaperCorrect);
            Assert.True(result.IsValid);
        }

        [TestCase(null)]
        [TestCase("")]
        public void Validate_NameOfThePublishingHouseNullOrEmpty_ReturnError(string nameOfThePublishingHouse)
        {
            _newspaperCorrect.NameOfThePublishingHouse = nameOfThePublishingHouse;
            var result = _newspaperValidator.TestValidate(_newspaperCorrect);
            result.ShouldHaveValidationErrorFor(newspaper => newspaper.NameOfThePublishingHouse);
        }

        [TestCase(-1)]
        [TestCase(0)]
        public void Validate_NumberOfInstancesLessOrEqual0_ReturnError(int numberOfInstances)
        {
            _newspaperCorrect.NumberOfInstances = numberOfInstances;
            var result = _newspaperValidator.TestValidate(_newspaperCorrect);
            result.ShouldHaveValidationErrorFor(newspaper => newspaper.NumberOfInstances);
        }

        [Test]
        public void Validate_NumberOfInstances1_ReturnNotError()
        {
            _newspaperCorrect.NumberOfInstances = 1;
            var result = _newspaperValidator.TestValidate(_newspaperCorrect);
            result.ShouldNotHaveValidationErrorFor(newspaper => newspaper.NumberOfInstances);
        }

        [TestCase(-1)]
        [TestCase(0)]
        public void Validate_NumberLessOrEqual0_ReturnError(int number)
        {
            _newspaperCorrect.Number = number;
            var result = _newspaperValidator.TestValidate(_newspaperCorrect);
            result.ShouldHaveValidationErrorFor(newspaper => newspaper.Number);
        }

        [Test]
        public void Validate_Number1_ReturnNotError()
        {
            _newspaperCorrect.Number = 1;
            var result = _newspaperValidator.TestValidate(_newspaperCorrect);
            result.ShouldNotHaveValidationErrorFor(newspaper => newspaper.Number);
        }

        [Test]
        public void Validate_ReleaseDateDefaultValue_ReturnError()
        {
            _newspaperCorrect.ReleaseDate = default;
            var result = _newspaperValidator.TestValidate(_newspaperCorrect);
            result.ShouldHaveValidationErrorFor(newspaper => newspaper.ReleaseDate);
        }

        [Test]
        public void Validate_ReleaseDateMoreDefaultValue_ReturnNotError()
        {
            _newspaperCorrect.ReleaseDate = default(DateTime).AddSeconds(1);
            var result = _newspaperValidator.TestValidate(_newspaperCorrect);
            result.ShouldNotHaveValidationErrorFor(newspaper => newspaper.ReleaseDate);
        }

        [TestCase("-1234-1234")]
        [TestCase("123-1234")]
        [TestCase("1234-123")]
        [TestCase("12345-1234")]
        [TestCase("1234-12345")]
        [TestCase("1234-1234-")]
        public void Validate_IssnIncorrectlySeparatedDashesOrQuantityNotEqual8_ReturnError(string issn)
        {
            _newspaperCorrect.Issn = issn;
            var result = _newspaperValidator.TestValidate(_newspaperCorrect);
            result.ShouldHaveValidationErrorFor(newspaper => newspaper.Issn);
        }

        [TestCase("123a-1234")]
        [TestCase("1234-123a")]
        public void Validate_IssnContainNotOnlyDigits_ReturnError(string issn)
        {
            _newspaperCorrect.Issn = issn;
            var result = _newspaperValidator.TestValidate(_newspaperCorrect);
            result.ShouldHaveValidationErrorFor(newspaper => newspaper.Issn);
        }

        [TestCase("1234-123X")]
        [TestCase("1234-1231")]
        public void Validate_IsbnLastDigitIsNumberOrX_ReturnNotError(string isbn)
        {
            _newspaperCorrect.Issn = isbn;
            var result = _newspaperValidator.TestValidate(_newspaperCorrect);
            result.ShouldNotHaveValidationErrorFor(newspaper => newspaper.Issn);
        }
    }
}
