﻿using ADO.Net.Dal;

namespace ADO.Net.Test.DalTest
{
    public class AdoOrderRepositoryTest : OrderRepositoriesTest
    {
        public AdoOrderRepositoryTest()
        {
            Repository = new AdoOrderRepository(_providerFactory, _connectionString);
        }
    }
}
