﻿using Introduction.Results;
using Introduction.LibraryModels;

namespace Introduction.Interfaces
{
    public interface IValidator
    {
        public ResultOfValidation IsValidItem(LibraryItem item);
    }
}
