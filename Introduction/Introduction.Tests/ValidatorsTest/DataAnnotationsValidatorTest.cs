﻿using Introduction.LibraryModels;
using Introduction.Validators;
using NUnit.Framework;
using System;

namespace Introduction.Tests.ValidatorsTest
{
    public class DataAnnotationsValidatorTest
    {
        private DataAnnotationsValidator _validator;

        [SetUp]
        public void Setup()
        {
            _validator = new();
        }

        [Test]
        public void IsValidItem_NotCorrectBook_NotEmptyListErrorsAndNotValid()
        {
            var tempBook = new Book()
            {
                Name = "Пиковая дама",
                Authors = { "кто-то", "Пушкин" },
                YearOfPublication = 1834,
                NameOfThePublishingHouse = "эксмо",
                PageCount = 200,
                Note = "Примечание",
                Price = 15.5m
            };
            var result = _validator.IsValidItem(tempBook);
            Assert.IsNotEmpty(result.ErrorMessages);
            Assert.False(result.IsValid);
        }

        [Test]
        public void IsValidItem_CorrectNewspaper_EmptyListErrorsAndValid()
        {
            var tempNewspaper = new Newspaper
            {
                Name = "Газета",
                PageCount = 50,
                Note = "примечание",
                Price = 100,
                NameOfThePublishingHouse = "Печать",
                Number = 3,
                YearOfPublication = 2000,
                NumberOfInstances = 5,
                ReleaseDate = new DateTime(2000, 7, 20)
            };
            var result = _validator.IsValidItem(tempNewspaper);
            Assert.IsEmpty(result.ErrorMessages);
            Assert.True(result.IsValid);
        }
    }
}
