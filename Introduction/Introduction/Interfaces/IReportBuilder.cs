﻿using Introduction.LibraryModels;
using System.Collections.Generic;

namespace Introduction.Interfaces
{
    public interface IReportBuilder
    {
        public void GenerateReport(string pathToReport, List<LibraryItem> catalogForRecord);
    }
}
