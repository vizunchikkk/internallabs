﻿<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
xmlns:msxsl="urn:schemas-microsoft-com:xslt"
xmlns="http://schemas.datacontract.org/2004/07/Introduction.LibraryModels">
  <xsl:output method="html" encoding="ISO-8859-1" indent="yes"/>
  <xsl:param name="date"/>

  <xsl:template match="/ArrayOfLibraryItem">
    <html>
      <body>
        <h2>
          Current Authors <xsl:value-of select="$date"/>
        </h2>
        <xsl:variable name="AuthorsWithoutBooks">
          <xsl:for-each select="LibraryItem/Authors/string[not(.=preceding::*)]">
            <Author>
              <xsl:value-of select="."/>
            </Author>
          </xsl:for-each>
        </xsl:variable>
        <xsl:variable name="Items">
          <xsl:copy-of select="/ArrayOfLibraryItem/*"/>
        </xsl:variable>
        <xsl:variable name="AuthorsWithBooks">
          <xsl:for-each select="msxsl:node-set($AuthorsWithoutBooks)/*">
            <Author>
              <xsl:variable name="TempAuthor">
                <xsl:value-of select="."/>
              </xsl:variable>
              <Name>
                <xsl:value-of select="."/>
              </Name>
              <Books>
                <xsl:for-each select="msxsl:node-set($Items)/LibraryItem">
                  <xsl:if test="Authors/string[contains(.,$TempAuthor)]">
                    <Book>
                      <xsl:value-of select="Name"/>
                    </Book>
                  </xsl:if>
                </xsl:for-each>
              </Books>
            </Author>
          </xsl:for-each>
        </xsl:variable>
        <table border="1">
          <tr bgcolor="#9acd32">
            <th>Name</th>
            <th>Books</th>
            <th>Total</th>
          </tr>
          <xsl:for-each select="msxsl:node-set($AuthorsWithBooks)/*">
            <tr>
              <td>
                <xsl:value-of select="*[name()='Name']"/>
              </td>
              <td>
                <xsl:for-each select="*/*[name()='Book']">
                  <xsl:value-of select="."/>;
                </xsl:for-each>
              </td>
              <td>
                <xsl:value-of select="count(*/*[name()='Book'])"/>
              </td>
            </tr>
          </xsl:for-each>
        </table>
        <p>
          Grand total: <xsl:value-of select="count(msxsl:node-set($AuthorsWithBooks)/*/*/*)"/>
        </p>
      </body>
    </html>
  </xsl:template>

</xsl:stylesheet>
