﻿namespace ADO.Net.Enums
{
    public enum OrderStatus
    {
        New,
        InProgress,
        Completed
    }
}
