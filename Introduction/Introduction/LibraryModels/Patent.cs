﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Introduction.LibraryModels
{
    [DataContract]
    public class Patent : LibraryItem
    {
        private DateTime _dateOfPublication;
        [DataMember]
        [MinLength(1, ErrorMessage = "Список изобретателей не может быть пустым")]
        public List<string> Inventor { get; set; }
        [DataMember]
        [Required(ErrorMessage = "Страна не может быть пустой")]
        public string Country { get; set; }
        [DataMember]
        [RegularExpression(@"^(((RE|PP|AI)\d{6})|((D|X|H|T)\d{7})|(\d{8})|(\d{1,10}-\d{4}\/\d{1,10}))$", ErrorMessage = "Номер патента должне иметь длинну 8 символов и может начинаться с букв RE или PP или D или AI или X или H или T, далее дополняется цифрами до 8 символов либо без букв и только 8 цифр, либо же иметь формат <номер> -<год>/<редакция>, где номер и редакция – целые числа, год представлен в формате 4 цифр")]
        public string RegistrationNumber { get; set; }
        [DataMember]
        [Range(typeof(DateTime), "01/01/1950", "01/01/2100", ErrorMessage = "Дата подачи заявки не может быть пустой и меньше 1950 года")]
        public DateTime ApplicationSubmissionDate { get; set; }
        [DataMember]
        [Range(typeof(DateTime), "01/01/1950", "01/01/2100", ErrorMessage = "Дата публикации не может быть пустой и меньше 1950 года")]
        public DateTime DateOfPublication
        {
            get
            {
                return _dateOfPublication;
            }
            set
            {
                _dateOfPublication = value;
                YearOfPublication = value.Year;
            }
        }

        public Patent()
        {
            Inventor = new List<string>();
        }
    }
}
