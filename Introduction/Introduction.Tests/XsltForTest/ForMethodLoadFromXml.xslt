﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" 
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xml:space="preserve">
    <xsl:output method="xml" indent="yes" omit-xml-declaration="yes"/>

  <xsl:template match="/ArrayOfLibraryItem/LibraryItem">
      <Id><xsl:value-of select="Id"/></Id>
      <Name><xsl:value-of select="Name"/></Name>
      <PageCount><xsl:value-of select="PageCount"/></PageCount>
      <Note><xsl:value-of select="Note"/></Note>
      <YearOfPublication><xsl:value-of select="YearOfPublication"/></YearOfPublication>
      <Price><xsl:value-of select="Price"/></Price>
    </xsl:template>
  

</xsl:stylesheet>
